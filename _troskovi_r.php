<?php
	include 'header.php';
	include 'controller/troskovi.php';
	include 'controller/zajednica.php';
	include 'controller/clanovi.php';
	$zajednica = new zajednica();
	$troskovi = new troskovi();
	$clanovi = new clanovi();

	if(strstr($_SERVER['REQUEST_URI'], 'action'))
		$troskovi -> $_GET['action']();
	if(strstr($_SERVER['REQUEST_URI'], 'join'))
		$clanovi-> $_GET['join']();

	// Print out the value of some variables
	$log->LogDebug("Pristup stranici: _troskovi_r.php");
?>

<div id="wrap">
<script type="text/javascript">
$().ready(function(){

	$('.search_div').hide();
	$('#id_zajednice').hide();

	$('.anchor_div').click(function(){

		
		$('.search_div').slideToggle("fast");
		$('.search_div').css("height", "200px");

	});

	$('.input_search').keyup(function(){

			var naziv = $(this).val();

			console.log(naziv);

	        var form_data = {
	            'naziv': naziv,
	            'ajax': '1'     
	        };
	        
	        $.post("controller/ajax_search_z.php", form_data, function(data){

	        	if(data)
	        	{
	        		$('.search_result').html(data);
	        		$('.search_result').css("overflow-y", "scroll");
	        		$('.search_result').css("height", "200px");
	        		$('.search_result').css("width", "100%");
	        	} else {
	        		$('.search_result').html('');
	        	}
	        });
	});

});

</script>
		<div id="container">
			<div id="content_main">
				<h1>Troškovi</h1>
				
			</div>
			<div id="content_left">
				<h1>Pregled troškova</h1>
				<ul>
					<? $troskovi -> ispis(); ?>
				</ul>
				<div id="content_left_bottom">
					
				</div>
			</div>
			<div id="content_right">
				<h1>Prijavite se u zajednicu</h1>
				<div class="search_div">
					<input type="input" class="input_search" style="margin-left: 80px;">
					<div class="search_result" style="margin-left: 80px;"></div>
				</div>
				<a class="anchor_div submit_button" href="#" style="margin-left: 80px;">Pretraga zajednica</a>
				<div id="content_right_bottom">
					
				</div>
			</div>
		</div>
		<div class="clearfooter"></div>
	</div>

	<?php
		$smarty->display('footer.tpl');
		//include 'footer.php';
	?>
	