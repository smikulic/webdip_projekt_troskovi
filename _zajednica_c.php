<?php
	include 'header.php';
	include 'controller/zajednica.php';
	$zajednica = new zajednica();

	if(strstr($_SERVER['REQUEST_URI'], 'action'))
		$zajednica -> $_GET['action']($_GET['id_zajednice']);

	// Print out the value of some variables
	$log->LogDebug("Pristup stranici: _zajednica_c.php");
?>

	<script type="text/javascript">
	$().ready(function() {

		$("input[name='naziv']").keyup(function(){

			var naziv = $(this).val();

			console.log(naziv);

	        var form_data = {
	            'naziv': naziv,
	            'ajax': '1'     
	        };

          	$.post("controller/ajax.php", form_data, function(data){

        		if(data)
        		{
        			$('.error').text(data);
        			$("input[type='submit']").disabled = true;
        		} else {
        			$('.error').text('');
        			$("input[type='submit']").disabled = false;
        		}
	        	
	        });
	    });
    });
	</script>


	<div id="wrap">

		<div id="container">
			<div id="content_main">
				<h1>Zajednice</h1>
				<p>Kreirajte novu zajednicu!</p>
			</div>
			<div id="content_left">
				<h1>Unos nove zajednice</h1>
				<? if(isset($_SESSION['username'])){ ?>
				<form action="_zajednica_c.php?action=upis" method="post" name="forma" enctype="multipart/form-data">
					<table>
						<tr>
							<td class="form_left"><label>Naziv zajednice:</label></td>
						</tr>
						<tr>
							<td><input class="form_right" type="text" name="naziv" id="naziv" value="Naziv..." onfocus="setValue(this)" onblur="setValue(this)"></td>
						</tr>
						<span class="error"></span>
						<tr>
							<td class="form_left"><input class="submit_button" type="submit" value="Unesi"></td>
						</tr>
					</table>
				</form>
				<input type="hidden" value="1" id="theValue" name="theValue"/>
				<?} else {?>
				<div>
					<p>Morate biti prijavljeni kako bi unjeli novu zajednicu</p>
					<form action="_zajednica_c.php?login=log_in" method="post" name="forma">
						<table>
							<tr>
								<td class="form_left"><label>Korisničko ime:</label></td>
							</tr>
							<tr>
								<td><input class="form_right" type="text" name="username" id="username" value="Korisničko ime..." onfocus="setValue(this)" onblur="setValue(this)"></td>
							</tr>
							<tr>
								<td class="form_left"><label>Lozinka:</label></td>
							</tr>
							<tr>
								<td><input class="form_right" type="password" name="password" id="password" value="Lozinka..." onfocus="setValue(this)" onblur="setValue(this)"></td>
							</tr>
							<tr>
								<td>
									<input class="submit_button" type="submit" value="Prijava">
								</td>
							</tr>
							<?// echo $_GET['']; ?>
						</table>
					</form>
				</div>
				<?}?>
				<div id="content_left_bottom">
					<h1>Pregled zajednica</h1>
					<ul>
						<?php
							//error_reporting (E_ERROR | E_WARNING | E_PARSE);
							$pagenum = $_GET['pagenum'];
							/*$dbc = mysql_connect("localhost", "WebDiP2011_068", "admin_I62n");*/
		    				$dbc = mysql_connect("localhost", "root", "");
							if (!$dbc) 
							{
								die ('Neuspješno spajanje: ' . mysql_error());
							}
							mysql_select_db("WebDiP2011_068", $dbc);
							mysql_query("set names utf8");	
							
							// Straničenje
							if (!(isset($pagenum)))
							{
								$pagenum = 1;
							}
							$data = mysql_query("SELECT * FROM zajednica") or die(mysql_error());
							$rows = mysql_num_rows($data);
							$page_rows = 10;
							$last = ceil($rows/$page_rows);
							if ($pagenum < 1)
							{
								$pagenum = 1;
							}
							elseif ($pagenum > $last) 
							{
								$pagenum = $last;
							}
							$max = 'limit ' .($pagenum - 1) * $page_rows .',' .$page_rows;
							
							$result = mysql_query("SELECT * FROM zajednica ORDER BY ukupni_trosak DESC $max") or die(mysql_error());
							
							$korisnik = new korisnik();

					        echo '<table border="0" width="100%" cellspacing="0" cellpadding="0">
					                <tr>
					                    <th align="left">Naziv zajednice</th>
					                    <th align="left">Ukupni trošak</th>
					                    <th align="left">Voditelj</th>
					                </tr>';

					        $i = 0;
					        while(($row = mysql_fetch_array($result)) !== false){
					            $i++;
					            echo '<tr class=\'red'.($i & 1).'\'>
					                    <td align="justify" style="color: #EC5F01; font-size: 24px; letter-spacing: 1px;" >
					                        <a class="content" href="_zajednica_r.php?action=ispis_one&id_zajednice='.$row[0].'">'.$row[2].'</a>
					                    </td>
					                    <td align="justify" >
					                        ' . $row[3] . ' kn
					                    </td>
					                    <td>
					                        ';  if(is_null($row[1]))
					                                echo'Nema voditelja';
					                            else 
					                                $korisnik -> username($row[1]); 
					                    echo'</td>
					                 </tr>';
					        }
					        echo'</table>';
							
							echo "<br/>";
							
							// Straničenje - pozicija stranice
							echo '<div id="stranice">';
							if ($pagenum == 1) 
							{
								echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
							} 
							else 
							{
								echo " <a class='content' href='{$_SERVER['PHP_SELF']}?pagenum=1'>POČETNA</a> ";
								echo " ";
								$previous = $pagenum - 1;
								echo " <a class='content' href='{$_SERVER['PHP_SELF']}?pagenum=$previous'>" . 
								'<img src="img/prethodna2.png" class="stranice"></img>' . "</a> ";
							}
							
							echo " $pagenum od $last ";
							
							if ($pagenum == $last) 
							{
								
							} 
							else 
							{
								$next = $pagenum + 1;
								echo " <a class='content' href='{$_SERVER['PHP_SELF']}?pagenum=$next'>" . 
								'<img src="img/arrow2.png" class="stranice"></img>' . "</a> ";
								echo " ";
								echo " <a class='content' href='{$_SERVER['PHP_SELF']}?pagenum=$last'>ZADNJA</a> ";
							}
							echo '</div>';
							// Straničenje - pozicija stranice
							// Straničenje KRAJ
							
							mysql_close($dbc);
						?>
					</ul>
				</div>
			</div>
			<div id="content_right">
				<? if(isset($_SESSION['username'])){ ?>
					<h1>Prijavite se u zajednicu</h1>
					<div class="search_div">
						<input type="input" class="input_search" style="margin-left: 80px;">
						<div class="search_result" style="margin-left: 80px;"></div>
					</div>
					<a class="anchor_div submit_button" href="#" style="margin-left: 80px;">Pretraga zajednica</a>
					<div id="content_right_bottom">
						
					</div>
				<?} else {?>
					<h1>Prijavite se u zajednicu</h1>
					<p>Morate biti prijavljeni za pristup zajednici</p>
				<?}?>
			</div>
		</div>
		<div class="clearfooter"></div>
	</div>

	<?php
		$smarty->display('footer.tpl');
		//include 'footer.php';
	?>
