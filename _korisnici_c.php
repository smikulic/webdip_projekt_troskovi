<?php
	include 'header.php';
	include 'controller/registracija.php';
	$registracija = new registracija();

	if(strstr($_SERVER['REQUEST_URI'], 'action'))
				$registracija -> $_GET['action']();

	// Print out the value of some variables
	$log->LogDebug("Pristup stranici: _korisnici_c.php");
?>

<script type="text/javascript" src="js/troskovi.js"></script>
	<div id="wrap">

		<div id="container">
			<div id="content_main">
				<h1>Konfiguracija - Dodavanje korisnika</h1>

			</div>
			<div id="content_left">
				<h1>Novi korisnik</h1>
				<ul>
					<script type="text/javascript">
					    $().ready(function() {

					        $("input[name='korisnicko_ime']").keyup(function(){

					            var korisnicko_ime = $(this).val();

					            console.log(korisnicko_ime);

					            var form_data = {
					                'korisnicko_ime': korisnicko_ime,
					                'ajax': '1'     
					            };
					            
					            $.post("controller/ajax_registracija.php", form_data, function(data){

					                if(data)
					                {
					                    $('.error').text(data);
					                    $("input[type='submit']").disabled = true;
					                } else {
					                    $('.error').text('');
					                    $("input[type='submit']").disabled = false;
					                }
					                    

					            });
					        });
					    });
					</script>

					<form id="registracija" enctype="multipart/form-data" action="_registracija.php?action=upis_admin" method="post">
					    <div class="form_left form_left_reg">
					        <fieldset class="inputs">
					            <label for="korisnicko_ime">Korisničko ime:</label>
					            <input name="korisnicko_ime" id="korisnicko_ime" type="text" value="Korisničko ime" onfocus="setValue(this)" onblur="setValue(this)" />
					            <span class="validacija error" id="korisnicko_imejava"></span>
					        </fieldset>
					        <fieldset class="inputs">
					            <label for="lozinka">Lozinka:</label>
					            <input name="lozinka" id="lozinka" type="lozinka" value="Lozinka" onfocus="setValue(this)" onblur="setValue(this)" />
					            <span class="validacija" id="lozinkajava"></span>
					        </fieldset>
					        <fieldset class="inputs">
					            <label for="lozinka2">Potvrda lozinke:</label>
					            <input name="lozinka2" id="lozinka2" type="lozinka2" value="Potvrda lozinke" onfocus="setValue(this)" onblur="setValue(this)" />
					            <span class="validacija" id="lozinka2java"></span>
					        </fieldset>
					        <fieldset class="inputs">
					            <label for="ime">Ime:</label>
					            <input name="ime" id="ime" type="text" value="Ime" onfocus="setValue(this)" onblur="setValue(this)" />
					            <span class="validacija" id="imejava"></span>
					        </fieldset>
					        <fieldset class="inputs">
					            <label for="prezime">Prezime:</label>
					            <input name="prezime" id="prezime" type="text" value="Prezime" onfocus="setValue(this)" onblur="setValue(this)" />
					            <span class="validacija" id="prezimejava"></span>
					        </fieldset>
					        <fieldset class="inputs">
					            <label for="email">E-mail:</label>
					            <input name="email" id="email" type="text" value="E-mail" onfocus="setValue(this)" onblur="setValue(this)" />
					            <span class="validacija" id="emailjava"></span>
					        </fieldset>            
					        <br />
					        <fieldset id="actions" style="margin-left: 267px; margin-top:-10px;">
					            <input type="submit" name="submit" class="submit_button" value="Dodaj korisnika" />
					        </fieldset>
					        <br /><br />
					    </div>
					</form>
				</ul>
				<div id="content_left_bottom">
					
				</div>
			</div>
			<div id="content_right">
				
				<div id="content_right_bottom">
					
				</div>
			</div>
		</div>
		<div class="clearfooter"></div>
	</div>

	<?php
		$smarty->display('footer.tpl');
		//include 'footer.php';
	?>
