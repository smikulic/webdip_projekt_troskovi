<?php
	include 'header.php';
	// Print out the value of some variables
	$log->LogDebug("Pristup stranici: _dokumentacija.php");
?>

<script type="text/javascript">
	// This method is in the core of the library, so we don't have to use() any
	// additional modules to access it.  However, this example requires 'node'.
	YUI().use('node', function(Y) {
	var results = Y.one('#demo'), ua = '';

	Y.each(Y.UA, function(v, k) {
	    if (!Y.Lang.isFunction(v)) {
	        var info = k + ': ' + v;
	        results.append('<p>' + info + '</p>');

	        if (v) {
	            if (Y.Lang.isNumber(v)) {
	                ua = info;
	            }
	        }
	    }
	});

	results.append('<p>Your browser is ' + ua + '</p>');
	   
	});
</script>

	<div id="wrap">
		<div id="container">
			<div id="content_main">
				<h1>Dokumentacija</h1>
			</div>
			<div id="content_left">
				<h2>Opis projektnog zadatka</h2>
				<p align="justify">
					Praćenje i evidencija primitaka/izdataka na razini osobe/zajednice. Sustav omogućuje praćenje troškova za određeni
					period, te treba imati mogućnosti za više tipova korisnika, tj. gosta, običnog korisnika, voditelja i administratora.
					Gost može pregledavati osnovne funkcionalnosti (troškovi, zajednice, troškovi po korisnicima za određeni period) te
					ima mogućnost prijave ili registracije. Korisnik osim toga može komentirati i ocjenjivati troškove, prijavljivati se
					u zajednicu, unositi vlastite troškove i tipove troška, te ukoliko kreira novu zajednicu postaje voditelj te zajednice. 
					Kao voditelj također može odobravati ostale korisnike u zajednicu. Administrator treba imati prostor za uređivanje i
					pregled podataka o korisnicima, te mogućnost mijenjanja statusa, brisanja korisnika ili resetiranja neuspješnih prijava.
				</p>
				<h2>Opis projektnog rješenja</h2>
				<p align="justify">
					Praćenje i evidencija troškova na razini osobe i zajednice. Sustav omogućuje praćenje troškova za određene 
					vremenske periode. Ima više tipova korisnika, administrator, voditelj, korisnik i gost. Gost može pregledavati 
					troškove po zajednicama, po osobi za dani period, te također može komentirati troškove ili se registrirati/prijavit
					u sustav. Korisnik ima sve mogućnosti kao i gost uz dodatak da se može prijavljivati u zajednice i ocjenjivati troškove.
					Osim toga, može kreirati vlastiti tip troška i iznos, te voditi vlastitu evidenciju troškova, kao i postati voditelj 
					kreiranjem nove zajednice. 
					Voditelj može kreirati zajednice i odobravati pristup drugim korisnicima, te također sve funkcionalnosti koje vrijede 
					za korisnika. Administrator ima pristup svakoj funkcionalnosti aplikacije, te može uređivati statuse korisnika, 
					kao i resetirati broj neuspješnih prijava. 
				</p>
				<h2>Odrednice projektnog rješenja</h2>
				<div style="float:left;">
					<h3>ERA model</h3>
					<a href="img/era.png"><img src="img/era.png" alt="ERA model" width="325px" height="300px" /></a>
				</div>
				<div style="float:right;">
					<h3>Use Case</h3>
					<a href="img/usecase.png"><img src="img/usecase.png" alt="Use Case" width="325px" height="300px" /></a>
				</div>
				<div style="float:left;">
					<h3>Dijagram sekvenci</h3>
					<a href="img/sekvence.png"><img src="img/sekvence.png" alt="Dijagram sekvenci" width="325px" height="300px" /></a>
				</div>
				<div style="float:right;">
					<h3>Navigacijski dijagram</h3>
					<a href="img/navigacijski.png"><img src="img/navigacijski.png" alt="Navigacijski dijagram" width="325px" height="300px" /></a>
				</div>
				<div style="float:left;">
					<h3>Browser info</h3>
					<div id="demo"></div>
				</div>
			
				<div id="content_left_bottom">

				</div>

			</div>
			<div id="content_right">
				<h2>Popis i opis skripti</h2>
				<p align="justify">
					Skripte sam razdvojio na view i controller strukturu, gdje se skripte za prikaz sadržaja (view) pozivaju preko controllera, te 
					također pozivaju funkcije unutar controllera koji komunicira sa config/db.php skriptom u kojoj se sadrže svi upisi/ispisi i ostale 
					manipulacije baze podataka.
				</p>
				<ul>View skripte: 
					<li>index.php - početna stranica</li>
					<li>header.php, footer.php - zaglavlje i podnožje</li>
					<li>_login.php, _registracija.php, forma.php - prijava i registracija korisnika</li>
					<li>_zajednica_c.php, _zajednica_r.php, _zajednica_c_admin.php, _zajednica_rud_admin.php - ispis/upis i pregled zajednica</li>
					<li>_troskovi_c.php, _troskovi_r.php - unos troškova</li>
					<li>_trosak_r.php, _trosak_clan_r.php, _trosak_zajednica_r.php, _troskovi_c_admin.php, _troskovi_rud_admin.php - detaljan pregled troškova po korisnicima/zajednicama</li>
					<li>_user.php, _postavke.php - konfiguracija sustava za korisnika i admina</li>
				</ul>
				<h2>Popis i opis korištenih tehnologija i alata</h2>
				<p align="justify">
					Koristio sam HTML5, CSS3, JavaScript, jQuery, Ajax, PHP, MYSQL, .htaccess i Smarty tehnologije, konkretno Cross-Browser / Cross-Platform razvoj, Regular Expressions, te osnovne SEO tehnike.
					Od alata sam koristio Sublime Text 2 kao osnovni editor, WAMP localhost, phpmyadmin, te FileZillu za upload i alate poput Web 
					Developer plugin-a za Chrome preglednik. Uz Chrome sam koristio i Firefox preglednik.
				</p>
				<ul>
					<li>HTML5 / CSS3</li>
					<li>javaScript / Ajax</li>
					<li>jQuery</li>
					<li>PHP / MYSQL</li>
					<li>.htaccess</li>
					<li>Smarty</li>
					<li>Regular Expressions</li>
					<li>WAMP</li>
					<li>Chrome preglednik / Web Developer plugin</li>
					<li>Firefox preglednik</li>
				</ul>
				<h2>Popis i opis vanjskih modula</h2>
				<p align="justify">
					Što se tiće HTML5 markup-a sam koristio popularniji HTML5 boilerplate framework principe, no ne i samu implementaciju, što također
					vrijedi i za sam PHP kod pisan po strukturi Codeigniter frameworka, dok je CSS stiliziranje nastalo po idejama iz Bootstrap-a, koji 
					koristim za rapid web development u poslu, pa samim time i JavaScript datoteke (http://code.jquery.com/jquery-latest.js, 
					jquery-ui-1.8.20.custom.min.js, jquery.raty.min.js, jquery-ui-1.8.20.custom.css) koji mi služe prvenstveno za Ajax, sustav ocjenjivanja i automatsko brisanje sadržaja unutar formi).
				</p>
				<h6>Ostale JS biblioteke</h6>
				<ul>
					<li>jquery.session.js, jquery.cookie.js - služe prilikom konfiguracije boja zaglavlja, tj. spremanje u cookie i čuvanje konfiguracije prilikom sljedećeg korištenja</li>
					<li>jTable.js - admin konfiguracija</li>
					<li>YUI - js framework</li>
				</ul>
			</div>
		</div>
		<div class="clearfooter"></div>
	</div>

	<?php
		$smarty->display('footer.tpl');
		//include 'footer.php';
	?>