<?php
	include 'header.php';
	// Print out the value of some variables
	$log->LogDebug("Pristup stranici: _korisnici_crud_admin.php");
?>

	<script type="text/javascript">
	    $(document).ready(function () {
	        $('#PersonTableContainer').jtable({
	            title: 'Korisnici',
	            actions: {
	                listAction: 'controller/jtable/list_korisnici.php',
	                createAction: 'controller/jtable/create_korisnici.php',
	                updateAction: 'controller/jtable/update_korisnici.php',
	                deleteAction: 'controller/jtable/delete_korisnici.php'
	            },		
	            fields: {
	                id_korisnik: {
	                    key: true,
	                    list: false
	                },
	                korisnicko_ime: {
	                    title: 'K.Ime',
	                    width: '15%'
	                },
	                lozinka: {
	                    title: 'Lozinka',
	                    width: '20%'
	                },
	                ime: {
	                    title: 'Ime',
	                    width: '15%'
	                },
	                prezime: {
	                    title: 'Prezime',
	                    width: '15%'
	                },
	                email: {
	                    title: 'E-mail',
	                    width: '20%'
	                },
	                neuspjesne_prijave: {
	                    title: 'N.Prijave',
	                    width: '15%'
	                }
	            }
	        });
	        $('#PersonTableContainer').jtable('load');
	    });
	</script>

	<div id="wrap">

		<div id="container">
			<div id="content_main">
				<h1>Konfiguracija - Korisnici</h1>
			</div>
			<div id="content_left">
				
				<div id="content_left_bottom">
					
					<div id="PersonTableContainer"></div>
					
				</div>
			</div>
			<div id="content_right" class="edit-naziv">

					<div id="content_right_bottom">
						
					</div>
			</div>
		</div>
		<div class="clearfooter"></div>
	</div>

	<?php
		$smarty->display('footer.tpl');
		//include 'footer.php';
	?>
