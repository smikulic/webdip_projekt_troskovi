<?php
	include 'header.php';

	// Print out the value of some variables
	$log->LogDebug("Pristup stranici: _troskovi_rud_admin.php");
?>

	<script type="text/javascript">
	    $(document).ready(function () {
	        $('#PersonTableContainer').jtable({
	            title: 'Kategorije troška',
	            actions: {
	                listAction: 'controller/jtable/list_tip.php',
	                createAction: 'controller/jtable/create_tip.php',
	                updateAction: 'controller/jtable/update_tip.php',
	                deleteAction: 'controller/jtable/delete_tip.php'
	            },		
	            fields: {
	                id_troska: {
	                    key: true,
	                    list: false
	                },
	                naziv: {
	                    title: 'Naziv',
	                    width: '100%'
	                }
	            }
	        });
	        $('#PersonTableContainer').jtable('load');
	    });
	</script>

	<div id="wrap">

		<div id="container">
			<div id="content_main">
				<h1>Konfiguracija - Kategorije troška</h1>
			</div>
			<div id="content_left">
				
				<div id="content_left_bottom">
					
					<div id="PersonTableContainer"></div>
					
				</div>
			</div>
			<div id="content_right" class="edit-naziv">

					<div id="content_right_bottom">
						
					</div>
			</div>
		</div>
		<div class="clearfooter"></div>
	</div>

	<?php
		$smarty->display('footer.tpl');
		//include 'footer.php';
	?>
