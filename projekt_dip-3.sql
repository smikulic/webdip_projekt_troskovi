-- phpMyAdmin SQL Dump
-- version 3.5.0
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 31, 2012 at 05:42 PM
-- Server version: 5.5.23
-- PHP Version: 5.3.10

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `projekt_dip`
--

-- --------------------------------------------------------

--
-- Table structure for table `clanovi`
--

CREATE TABLE IF NOT EXISTS `clanovi` (
  `id_zajednica_clanovi` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_zajednica` int(10) unsigned DEFAULT NULL,
  `clan` int(10) unsigned DEFAULT NULL,
  `odobren` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_zajednica_clanovi`),
  KEY `clanovi_FKIndex1` (`clan`),
  KEY `clanovi_FKIndex2` (`id_zajednica`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=16 ;

--
-- Dumping data for table `clanovi`
--

INSERT INTO `clanovi` (`id_zajednica_clanovi`, `id_zajednica`, `clan`, `odobren`) VALUES
(1, 1, 1, 0),
(2, 1, 1, 0),
(3, 5, 1, 1),
(4, 5, 3, 1),
(5, 5, 4, 1),
(6, 5, 0, 0),
(7, 16, 4, 0),
(8, 1, 4, 0),
(9, 8, 4, 0),
(10, 8, 4, 0),
(11, 8, 4, 0),
(12, 18, 4, 1),
(13, 18, 3, 1),
(14, 18, 1, 1),
(15, 25, 4, 1);

-- --------------------------------------------------------

--
-- Table structure for table `komentar`
--

CREATE TABLE IF NOT EXISTS `komentar` (
  `id_komentar` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_trosak_clanovi` int(10) unsigned NOT NULL,
  `id_korisnik` int(10) unsigned NOT NULL,
  `datum` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `tekst` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id_komentar`),
  KEY `komentar_FKIndex1` (`id_korisnik`),
  KEY `komentar_FKIndex2` (`id_trosak_clanovi`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `komentar`
--

INSERT INTO `komentar` (`id_komentar`, `id_trosak_clanovi`, `id_korisnik`, `datum`, `tekst`) VALUES
(1, 7, 4, '2012-05-31 13:44:06', 'fdsfadsfads'),
(2, 7, 4, '2012-05-31 13:54:01', 'adsfadsfasdfasd'),
(3, 7, 1, '2012-05-31 14:05:41', 'Da da da i ja isto');

-- --------------------------------------------------------

--
-- Table structure for table `korisnik`
--

CREATE TABLE IF NOT EXISTS `korisnik` (
  `id_korisnik` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_tip` int(10) unsigned NOT NULL DEFAULT '1',
  `id_status` int(10) unsigned NOT NULL DEFAULT '0',
  `korisnicko_ime` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lozinka` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ime` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `prezime` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `aktivacijski_kod` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `neuspjesne_prijave` int(10) unsigned DEFAULT NULL,
  `blokiran_do` datetime DEFAULT NULL,
  `avatar` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id_korisnik`),
  KEY `korisnik_FKIndex1` (`id_tip`),
  KEY `korisnik_FKIndex2` (`id_status`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Dumping data for table `korisnik`
--

INSERT INTO `korisnik` (`id_korisnik`, `id_tip`, `id_status`, `korisnicko_ime`, `lozinka`, `ime`, `prezime`, `email`, `aktivacijski_kod`, `neuspjesne_prijave`, `blokiran_do`, `avatar`) VALUES
(1, 1, 1, 'smikulic', 'smikulic1337', 'Siniša', 'Mikulić', 'frut3k@hotmail.com', NULL, 0, NULL, NULL),
(3, 1, 1, 'vmarcetic', 'vmarcetic1337', 'Vedran', 'Marčetić', 'frut3k@hotmail.com', NULL, 0, NULL, NULL),
(4, 0, 1, 'admin', 'admin', 'Siniša', 'Mikulić', 'frut3k@hotmail.com', NULL, 0, NULL, NULL),
(6, 1, 0, 'fdas', 'Lozinka1', 'Ada', 'Prezime', 'a@a.hr', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `predlozak`
--

CREATE TABLE IF NOT EXISTS `predlozak` (
  `id_predlozak` int(10) unsigned NOT NULL,
  `id_korisnik` int(10) unsigned NOT NULL,
  `id_troska` int(10) unsigned DEFAULT NULL,
  `naziv` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id_predlozak`),
  KEY `predlozak_FKIndex1` (`id_korisnik`),
  KEY `predlozak_FKIndex2` (`id_troska`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `status_korisnika`
--

CREATE TABLE IF NOT EXISTS `status_korisnika` (
  `id_status` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `opis` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id_status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tip_korisnika`
--

CREATE TABLE IF NOT EXISTS `tip_korisnika` (
  `id_tip` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `naziv` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id_tip`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tip_korisnika`
--

INSERT INTO `tip_korisnika` (`id_tip`, `naziv`) VALUES
(0, 'admin'),
(1, 'korisnik');

-- --------------------------------------------------------

--
-- Table structure for table `tip_troska`
--

CREATE TABLE IF NOT EXISTS `tip_troska` (
  `id_troska` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `naziv` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id_troska`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=15 ;

--
-- Dumping data for table `tip_troska`
--

INSERT INTO `tip_troska` (`id_troska`, `naziv`) VALUES
(1, 'auto'),
(2, 'struja'),
(3, 'da'),
(4, 'plin'),
(5, 'izlazak'),
(6, ''),
(7, ''),
(8, 'gg'),
(9, 'gg2'),
(10, 'gg3'),
(11, 'gg4'),
(12, 'gg5'),
(13, 'gg6'),
(14, 'ggg');

-- --------------------------------------------------------

--
-- Table structure for table `trosak_clanovi`
--

CREATE TABLE IF NOT EXISTS `trosak_clanovi` (
  `id_trosak_clanovi` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_korisnik` int(10) unsigned NOT NULL,
  `id_troska` int(10) unsigned NOT NULL,
  `godina` int(10) unsigned DEFAULT NULL,
  `mjesec` int(10) unsigned DEFAULT NULL,
  `iznos` float DEFAULT NULL,
  `vrijeme_unosa` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ocjena` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_trosak_clanovi`),
  KEY `trosak_clanovi_FKIndex1` (`id_troska`),
  KEY `trosak_clanovi_FKIndex2` (`id_korisnik`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=15 ;

--
-- Dumping data for table `trosak_clanovi`
--

INSERT INTO `trosak_clanovi` (`id_trosak_clanovi`, `id_korisnik`, `id_troska`, `godina`, `mjesec`, `iznos`, `vrijeme_unosa`, `ocjena`) VALUES
(1, 1, 2, 21, 1, 1423, '2012-05-21 11:36:26', 0),
(2, 0, 1, 2010, 1, 2, '2012-05-21 13:33:06', 0),
(3, 0, 5, 2012, 5, 30, '2012-05-22 09:09:59', 0),
(4, 0, 1, 2010, 1, 300, '2012-05-28 15:20:59', 0),
(5, 0, 2, 2012, 5, 500, '2012-05-28 16:44:27', 0),
(6, 0, 4, 2012, 5, 500, '2012-05-28 16:46:06', 0),
(7, 4, 1, 2010, 1, 344, '2012-05-28 16:48:54', 4),
(8, 4, 1, 2010, 9, 500, '2012-05-28 18:04:11', 0),
(9, 4, 5, 2012, 5, 99, '2012-05-29 09:21:45', 0),
(10, 4, 1, 2011, 1, 42, '2012-05-29 09:47:35', 0),
(11, 4, 1, 2012, 5, 5000, '2012-05-29 11:40:51', 0),
(12, 4, 3, 2013, 4, 30000, '2012-05-29 11:51:04', 0),
(13, 4, 1, 2013, 5, 500, '2012-05-31 09:13:28', 0),
(14, 4, 1, 2012, 1, 600, '2012-05-31 10:04:52', 0);

-- --------------------------------------------------------

--
-- Table structure for table `trosak_godina`
--

CREATE TABLE IF NOT EXISTS `trosak_godina` (
  `id_trosak_godina` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_zajednica` int(10) unsigned NOT NULL DEFAULT '0',
  `godina` int(10) unsigned NOT NULL DEFAULT '0',
  `trosak` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_trosak_godina`),
  UNIQUE KEY `godina` (`godina`,`id_zajednica`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=41 ;

--
-- Dumping data for table `trosak_godina`
--

INSERT INTO `trosak_godina` (`id_trosak_godina`, `id_zajednica`, `godina`, `trosak`) VALUES
(7, 2, 3, 3000),
(23, 5, 2010, 5392),
(26, 0, 2010, 2000),
(34, 5, 2012, 1099),
(36, 5, 2011, 42),
(37, 18, 2012, 5600),
(38, 18, 2013, 30500);

-- --------------------------------------------------------

--
-- Table structure for table `trosak_mjesec`
--

CREATE TABLE IF NOT EXISTS `trosak_mjesec` (
  `id_trosak_mjesec` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_zajednica` int(10) unsigned DEFAULT NULL,
  `godina` int(10) unsigned DEFAULT NULL,
  `mjesec` int(10) unsigned DEFAULT NULL,
  `trosak` float DEFAULT NULL,
  PRIMARY KEY (`id_trosak_mjesec`),
  UNIQUE KEY `id_zajednica` (`id_zajednica`,`godina`,`mjesec`),
  KEY `trosak_mjesec_FKIndex1` (`id_zajednica`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=16 ;

--
-- Dumping data for table `trosak_mjesec`
--

INSERT INTO `trosak_mjesec` (`id_trosak_mjesec`, `id_zajednica`, `godina`, `mjesec`, `trosak`) VALUES
(2, 5, 2010, 1, 1638),
(6, 5, 2012, 5, 1099),
(9, 5, 2010, 9, 500),
(11, 5, 2011, 1, 42),
(12, 18, 2012, 5, 5000),
(13, 18, 2013, 4, 30000),
(14, 18, 2013, 5, 500),
(15, 18, 2012, 1, 600);

-- --------------------------------------------------------

--
-- Table structure for table `trosak_ocjene`
--

CREATE TABLE IF NOT EXISTS `trosak_ocjene` (
  `id_trosak_clanovi` int(11) NOT NULL,
  `id_korisnika` int(11) NOT NULL,
  `ocjena` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `trosak_ocjene`
--

INSERT INTO `trosak_ocjene` (`id_trosak_clanovi`, `id_korisnika`, `ocjena`) VALUES
(7, 4, 3),
(7, 4, 4),
(7, 4, 4),
(7, 1, 3);

-- --------------------------------------------------------

--
-- Table structure for table `zajednica`
--

CREATE TABLE IF NOT EXISTS `zajednica` (
  `id_zajednica` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_moderator` int(10) unsigned DEFAULT NULL,
  `naziv` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ukupni_trosak` float DEFAULT '0',
  PRIMARY KEY (`id_zajednica`),
  KEY `zajednica_FKIndex1` (`id_moderator`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=26 ;

--
-- Dumping data for table `zajednica`
--

INSERT INTO `zajednica` (`id_zajednica`, `id_moderator`, `naziv`, `ukupni_trosak`) VALUES
(1, 1, 'dadsada', 32423),
(2, 2, 'dadas', 432),
(3, 1, 'da', NULL),
(4, 1, 'dada', NULL),
(5, NULL, 'Kucanstvo 1', NULL),
(6, 1, 'da', NULL),
(7, 1, 'da', NULL),
(8, 2, 'Kucnastvo 4', NULL),
(9, 2, 'da', NULL),
(10, 1, 'da', NULL),
(11, 2, 'kucanstvo 1', NULL),
(12, 2, 'x', NULL),
(13, 1, 'Zajednica 1', NULL),
(14, 2, 'Zajednica 2', NULL),
(16, NULL, 'PeroJagode', NULL),
(17, 1, 'perobanana', NULL),
(18, 4, 'Firma 1', 86700),
(19, 4, 'admin', 0),
(20, 4, 'admin1', 0),
(21, 4, 'admin1', 0),
(22, 4, 'admin1', 0),
(23, 4, '', 0),
(24, 4, 'jÄlkÄj', 0),
(25, 4, 'gfdgdfhht5', 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
