<?php
	include 'header.php';
	include 'controller/registracija.php';
	$registracija = new registracija();

	if(strstr($_SERVER['REQUEST_URI'], 'action'))
				$registracija -> $_GET['action']();

	// Print out the value of some variables
	$log->LogDebug("Pristup stranici: _registracija.php");
?>

<script type="text/javascript" src="js/troskovi.js"></script>
	<div id="wrap">

		<div id="container">
			<div id="content_main">
				<h1>Registracija</h1>

			</div>
			<div id="content_left">
				<h1>Registrirajte se</h1>
				<ul>
					<?php
                        include 'forma.php'; 
                    ?>
				</ul>
				<div id="content_left_bottom">
					
				</div>
			</div>
			<div id="content_right">
				<h1>Unesite novu zajednicu</h1>
				<a class="button" href="_zajednica_c.php">Kreiraj novu zajednicu</a>
				<div id="content_right_bottom">
					<h1>Unesite novi trošak</h1>
					<a class="button" href="_troskovi_c.php">&nbsp;Kreiraj novi trošak &nbsp;&nbsp;</a>
				</div>
			</div>
		</div>
		<div class="clearfooter"></div>
	</div>

	<?php
		$smarty->display('footer.tpl');
		//include 'footer.php';
	?>
