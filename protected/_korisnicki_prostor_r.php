<!DOCTYPE html>
<?	error_reporting(0);if (!isset($_SESSION)) { session_start();
	}

	ob_start();

	include '../config/db.php';
 	include '../controller/login.php';
 	include '../controller/korisnik.php';
	$login = new login();

	if(strstr($_SERVER['REQUEST_URI'], '?login'))
		$login -> $_GET['login']();

?>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<script src="http://code.jquery.com/jquery-latest.js"></script>
	<script type="text/javascript" src="../js/jquery-ui-1.8.20.custom.min.js"></script>
	<script type="text/javascript" src="../js/jquery.raty.min.js"></script>
	<link rel="stylesheet" type="text/css" href="../css/custom-theme/jquery-ui-1.8.20.custom.css" media="all" />
    <title>Troškovi.hr</title>
    <link href="../css/style.css" rel="stylesheet">
    <link rel="shortcut icon" href="../img/favicon.png">
  	<link rel="apple-touch-icon" sizes="114x114" href="../img/icon.png">
</head>
<body>
	<div id="header">

	  	<div id="header_top">
		    <div id="logo">
		      <a href="../index.php"><img src="../img/logo2.png" alt="troškovi.hr" /></a>
		      <h1 id="logo_h1">Troškovi.hr</h1>
		      <h4 id="logo_h4">Evidencija troškova na razini kućanstva</h4>
		      <? 
		      	if($_SESSION['id'] == '1') {

		      		if(isset($_SESSION['id'])){ 
		      ?>
			<div class="login">Dobro došli <a class="login_link" href="_korisnicki_prostor_r.php"><?= $_SESSION['username'] ?></a>!&nbsp;
				<a class="login_link" style="float:right;" href="_zajednica_c.php?login=logout"> Logout</a></div>
			<?
				} }
				else {

					if(isset($_SESSION['id'])){
			?>
			<div class="login">Dobro došli <a class="login_link" href="../_login.php"><?= $_SESSION['username'] ?></a>!&nbsp;
				<a class="login_link" style="float:right;" href="../_zajednica_c.php?login=logout"> Logout</a></div>
			<? } } ?>
		    </div>
		    <div id="menu">
                <div id="menu_right">
                    <ul>
						<li><a class="menu" href="../index.php">Početna</a></li>
				    	<li><a class="menu" href="../_zajednica_c.php">Zajednice</a></li>
			    		<li><a class="menu" href="../_troskovi_r.php">Troškovi</a></li>
			    		<li><a class="menu" href="../dokumentacija.html">Dokumentacija</a></li>
		    			<li><a class="menu" href="../_login.php">Prijava/Registracija</a></li>
				    </ul>
                </div>
            </div>
	   	</div>
	   	
	    <div id="header_bottom">
	    	
	  	</div>

	</div>
<?php
	include '../controller/korisnicki_prostor.php';
	$korisnicki_prostor = new korisnicki_prostor();

	if(strstr($_SERVER['REQUEST_URI'], 'action'))
		$korisnicki_prostor -> $_GET['action']();

?>

<script type="text/javascript">
$().ready(function() {

	$('.reset_korisnika').click(function(){

		parent.location='_korisnicki_prostor_r.php?action=reset_korisnika&id_korisnik='+$(this).attr('id')+'';

	});

	$('.ban_korisnika').click(function(){

		parent.location='_korisnicki_prostor_r.php?action=ban_korisnika&id_korisnik='+$(this).attr('id')+'';

	});

	$('.odobri_korisnika').click(function(){

		parent.location='_korisnicki_prostor_r.php?action=odobri_korisnika&id_korisnik='+$(this).attr('id')+'';

	});

	$('.delete_korisnika').click(function(){
		var confirm_delete = confirm('Želite li izbrisati korisnika!')		
			if(confirm_delete){
				
				parent.location='_korisnicki_prostor_r.php?action=delete_korisnika&id_korisnik='+$(this).attr('id')+'';
				alert("Korisnik izbrisan!");
			
			} else {
				
				e.preventDefault();
			}
	});


});
</script>

<div id="wrap">
		<div id="container">
			<div id="content_main">
				
			</div>
			<div id="content_left">
				<h1>Administrator - Upravljanje sustavom</h1>
				<h2>Neuspješne prijave</h2>
				<ul>
					<? $korisnicki_prostor -> ispis_blokirani(); ?>
				</ul>
				<div id="content_left_bottom">
					<h2>Statusi korisnika</h2>
					<ul>
						<? $korisnicki_prostor -> ispis_korisnika(); ?>
					</ul>
				</div>
			</div>
			<div id="content_right">
				
				<div id="content_right_bottom">
					
				</div>
			</div>
		</div>
		<div class="clearfooter"></div>
	</div>

	<?php
		include '../footer.php';
	?>
	