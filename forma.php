<script type="text/javascript">
    $().ready(function() {

        $("input[name='korisnicko_ime']").keyup(function(){

            var korisnicko_ime = $(this).val();

            console.log(korisnicko_ime);

            var form_data = {
                'korisnicko_ime': korisnicko_ime,
                'ajax': '1'     
            };
            
            $.post("controller/ajax_registracija.php", form_data, function(data){

                if(data)
                {
                    $('.error').text(data);
                    $("input[type='submit']").disabled = true;
                } else {
                    $('.error').text('');
                    $("input[type='submit']").disabled = false;
                }
                    
            });
        });
    });
</script>

<form id="registracija" enctype="multipart/form-data" action="_registracija.php?action=upis" method="post">
    <div class="form_left form_left_reg">
        <fieldset class="inputs">
            <label for="korisnicko_ime">Korisničko ime:</label>
            <input name="korisnicko_ime" id="korisnicko_ime" type="text" value="Korisničko ime" onfocus="setValue(this)" onblur="setValue(this)" />
            <span class="validacija error" id="korisnicko_imejava"></span>
        </fieldset>
        <fieldset class="inputs">
            <label for="lozinka">Lozinka:</label>
            <input name="lozinka" id="lozinka" type="lozinka" value="Lozinka" onfocus="setValue(this)" onblur="setValue(this)" />
            <span class="validacija" id="lozinkajava"></span>
        </fieldset>
        <fieldset class="inputs">
            <label for="lozinka2">Potvrda lozinke:</label>
            <input name="lozinka2" id="lozinka2" type="lozinka2" value="Potvrda lozinke" onfocus="setValue(this)" onblur="setValue(this)" />
            <span class="validacija" id="lozinka2java"></span>
        </fieldset>
        <fieldset class="inputs">
            <label for="ime">Ime:</label>
            <input name="ime" id="ime" type="text" value="Ime" onfocus="setValue(this)" onblur="setValue(this)" />
            <span class="validacija" id="imejava"></span>
        </fieldset>
        <fieldset class="inputs">
            <label for="prezime">Prezime:</label>
            <input name="prezime" id="prezime" type="text" value="Prezime" onfocus="setValue(this)" onblur="setValue(this)" />
            <span class="validacija" id="prezimejava"></span>
        </fieldset>
        <fieldset class="inputs">
            <label for="email">E-mail:</label>
            <input name="email" id="email" type="text" value="E-mail" onfocus="setValue(this)" onblur="setValue(this)" />
            <span class="validacija" id="emailjava"></span>
        </fieldset>
            <label for="validacija">Validacija:</label>
            <div style="margin: -20px 0 0 170px;">
                <?php
                    require_once('config/recaptchalib.php');
                    $publickey = "6LfiXsMSAAAAAC5lERxM7h0lWVIShb2KBoJ2wGCQ";
                    echo recaptcha_get_html($publickey);
                ?>
            </div>             
        <br />
        <fieldset id="actions" style="margin-left: 267px; margin-top:-10px;">
            <input type="submit" name="submit" class="submit_button" value="Registrirajte se" />
        </fieldset>
        <br /><br />
    </div>
</form>