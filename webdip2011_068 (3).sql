-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 20, 2012 at 09:52 PM
-- Server version: 5.5.24-log
-- PHP Version: 5.4.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `webdip2011_068`
--

-- --------------------------------------------------------

--
-- Table structure for table `clanovi`
--

CREATE TABLE IF NOT EXISTS `clanovi` (
  `id_zajednica_clanovi` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_zajednica` int(10) unsigned DEFAULT NULL,
  `clan` int(10) unsigned DEFAULT NULL,
  `odobren` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_zajednica_clanovi`),
  KEY `clanovi_FKIndex1` (`clan`),
  KEY `clanovi_FKIndex2` (`id_zajednica`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=55 ;

--
-- Dumping data for table `clanovi`
--

INSERT INTO `clanovi` (`id_zajednica_clanovi`, `id_zajednica`, `clan`, `odobren`) VALUES
(27, 31, 2, 1),
(28, 32, 2, 1),
(29, 31, 3, 1),
(30, 33, 3, 1),
(31, 31, 4, 1),
(32, 31, 5, 0),
(33, 31, 11, 0),
(34, 34, 11, 1),
(35, 34, 4, 0),
(36, 34, 3, 0),
(37, 34, 2, 0),
(38, 35, 6, 1),
(39, 31, 6, 0),
(40, 36, 15, 1),
(41, 37, 17, 1),
(42, 38, 18, 1),
(43, 39, 2, 1),
(44, 35, 3, 0),
(45, 39, 3, 1),
(46, 40, 3, 1),
(47, 40, 2, 0),
(48, 41, 5, 1),
(49, 42, 11, 1),
(50, 43, 11, 1),
(51, 34, 25, 0),
(52, 44, 3, 1),
(53, 45, 1, 1),
(54, 46, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `komentar`
--

CREATE TABLE IF NOT EXISTS `komentar` (
  `id_komentar` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_trosak_clanovi` int(10) unsigned NOT NULL,
  `id_korisnik` int(10) unsigned NOT NULL,
  `datum` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `tekst` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id_komentar`),
  KEY `komentar_FKIndex1` (`id_korisnik`),
  KEY `komentar_FKIndex2` (`id_trosak_clanovi`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=19 ;

--
-- Dumping data for table `komentar`
--

INSERT INTO `komentar` (`id_komentar`, `id_trosak_clanovi`, `id_korisnik`, `datum`, `tekst`) VALUES
(9, 33, 4, '2012-06-01 19:31:17', 'Pa na što si potrošio 120kn...'),
(10, 33, 3, '2012-06-01 19:33:05', 'WC papir, šta misliš, ono lol'),
(11, 33, 2, '2012-06-01 19:34:38', 'Pa zar nije samo bero bio u 12.mj u VŽ?'),
(12, 33, 5, '2012-06-01 19:35:46', 'da, ali bio je i marsh.. i daj me više DODAJ u zajednicu!!'),
(13, 33, 3, '2012-06-01 19:45:25', 'AHAHAHAHAHAHAAHAA =)))))'),
(14, 33, 4, '2012-06-01 19:46:43', 'Daj prestanite trolat'),
(15, 34, 4, '2012-06-01 19:47:32', 'e ozano.....  bok =)'),
(16, 29, 12, '2012-06-01 19:49:02', 'a čeče plača stanarinu na vrijeme :)'),
(17, 29, 3, '2012-06-01 19:53:48', 'pff, pa da kad nisam stoka'),
(18, 16, 3, '2012-06-01 20:59:30', 'Bar bi se itko mogao najesti za 35kn =/');

-- --------------------------------------------------------

--
-- Table structure for table `korisnici`
--

CREATE TABLE IF NOT EXISTS `korisnici` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ime` varchar(30) NOT NULL,
  `prezime` varchar(30) NOT NULL,
  `email` varchar(30) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(30) NOT NULL,
  `status` int(11) NOT NULL,
  `Activation` varchar(40) NOT NULL,
  `avatar` varchar(120) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=35 ;

--
-- Dumping data for table `korisnici`
--

INSERT INTO `korisnici` (`id`, `ime`, `prezime`, `email`, `username`, `password`, `status`, `Activation`, `avatar`) VALUES
(1, 'Siniša', 'Mikulić', 'smikulic@foi.hr', 'smikulic', 'smikulic', 1, '', ''),
(2, 'Korisnik', 'Korisnik', 'Korisnik', 'korisnik', 'korisnik', 0, '', ''),
(3, 'Ime', 'Prezime', 'frut3k@hotmail.com', 'Korisnik1', 'Lozinka1', 0, '', ''),
(5, 'Marin', 'Zlatarić', 'frut3k@hotmail.com', 'mzlataric', 'Mzlataric1', 1, '', ''),
(6, 'Ivor', 'Reić', 'frut3k@hotmail.com', 'ireic', 'Ireic1', 0, '', ''),
(7, 'Ozano', 'Rokov', 'frut3k@hotmail.com', 'orokov', 'Orokov1', 1, '', ''),
(8, 'Vedran', 'Marčetić', 'frut3k@hotmail.com', 'vmarcetic', 'Vmarcetic1', 0, '', ''),
(9, 'Dave', 'Grohl', 'frut3k@hotmail.com', 'dgrohl', 'Dgrohl1', 1, '', ''),
(10, 'Barney', 'Stinson', 'frut3k@hotmail.com', 'bstinson', 'Bstinson1', 0, '', ''),
(11, 'Može', 'Može', 'moze@moze.com', 'moze', 'moze', 1, '', ''),
(12, 'GG', 'GG', 'gg@vgg.com', 'gg', 'vgg', 0, '', ''),
(13, 'Maja', 'Hornung', 'maja@hornung.com', 'mhornung', 'Mhornung1', 1, '', ''),
(14, 'Chuck', 'Norris', 'chuck@norris.com', 'cnorris', 'Cnorris1', 1, '', ''),
(15, 'Zvonko', 'Podbojec', 'frut3k@hotmail.com', 'zpodbojec', 'Zpodbojec1', 0, '', ''),
(16, 'Berislav', 'Šipek', 'frut3k@hotmail.com', 'bsipek', 'Bsipek1', 0, '', ''),
(17, 'Nenad', 'Nikolić', 'gg@gg.com', 'nnikolic', 'Lozinka1', 1, '', ''),
(18, 'Leet', '1337', 'frut3k@hotmail.com', '1337', '1337', 0, '', ''),
(19, 'Novi', 'Korisnik', 'korisnik@korisnik.com', 'korisnik', 'korisnik', 1, '', ''),
(20, 'Stewie', 'Griffin', 'stewie@griffin.com', 'sgriffin', 'Sgriffin1', 1, '', ''),
(21, 'test', 'test', 'test', 'test', 'test', 1, '', ''),
(22, 'Bodgan', 'Okreša-Đurić', 'frut3k@hotmail.com', 'bokresa', 'Bokresa1', 1, '', ''),
(23, 'Treća', 'Stranica', 'Ispisa', 'korisnika', 'korisnika', 0, '', ''),
(24, 'Novi001', 'Novi001', 'novi@001.com', 'Novi001', 'Lozinka1', 0, '49b5f9b9cb4e5e0b50bd7a1e566cc66f', ''),
(25, 'Novi002', 'Novi002', 'novi@002.com', 'Novi002', 'Lozinka1', 0, '34dd825e7c656d3ad2140d85265e8d64', ''),
(26, 'Novi003', 'Novi003', 'novi@003.com', 'Novi003', 'Lozinka1', 0, '0dc4d5664a89b0ec56ea0c6bab44b0e1', ''),
(27, 'Novi004', 'Novi004', 'novi@004.com', 'Novi004', 'Lozinka1', 0, '7cbea72262323afb5f0f7a32734ad3f7', ''),
(28, 'Novi005', 'Novi005', 'novi@005.com', 'Novi005', 'Lozinka1', 0, '71b50ce2a6723bd13ba6917baf63a537', ''),
(29, 'Novi006', 'Novi006', 'novi@006.com', 'Novi006', 'Lozinka1', 0, 'acc21d34e0f5c7292358e75b6e7da56d', ''),
(30, 'Ime', 'Prezime', 'E-mail', 'Korisničko ime', 'Lozinka1', 0, 'ed786fe43504652ef0673c044ede0a77', ''),
(31, 'User1337', 'User1337', 'frut3k@hotmail.com', 'User1337', 'Lozinka', 0, '203d6023f79697c3ffa95d8321caab1d', ''),
(32, 'User1337', 'User1337', 'frut3k@hotmail.com', 'User1337', 'User1337', 0, '04afb3c2f21116633e04ab3333bab2f7', ''),
(33, 'LOLuser1', 'LOLuser1', 'frut3k@hotmail.com', 'LOLuser1', 'LOLuser1', 0, 'e403a17a723dba182d78a1fa221dd596', ''),
(34, '', '', '', '', '', 0, '99c35d4b09358f79a733e90e8c062793', '');

-- --------------------------------------------------------

--
-- Table structure for table `korisnik`
--

CREATE TABLE IF NOT EXISTS `korisnik` (
  `id_korisnik` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_tip` int(10) unsigned NOT NULL DEFAULT '1',
  `id_status` int(10) unsigned NOT NULL DEFAULT '0',
  `korisnicko_ime` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `lozinka` varchar(30) CHARACTER SET utf8 DEFAULT NULL,
  `ime` varchar(30) CHARACTER SET utf8 DEFAULT NULL,
  `prezime` varchar(30) CHARACTER SET utf8 DEFAULT NULL,
  `email` varchar(30) CHARACTER SET utf8 DEFAULT NULL,
  `aktivacijski_kod` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `neuspjesne_prijave` int(10) unsigned DEFAULT NULL,
  `blokiran_do` datetime DEFAULT NULL,
  `avatar` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`id_korisnik`),
  KEY `korisnik_FKIndex1` (`id_tip`),
  KEY `korisnik_FKIndex2` (`id_status`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=29 ;

--
-- Dumping data for table `korisnik`
--

INSERT INTO `korisnik` (`id_korisnik`, `id_tip`, `id_status`, `korisnicko_ime`, `lozinka`, `ime`, `prezime`, `email`, `aktivacijski_kod`, `neuspjesne_prijave`, `blokiran_do`, `avatar`) VALUES
(1, 0, 1, 'admin', 'admin', 'Siniša', 'Mikulić', 'frut3k@hotmail.com', NULL, 0, NULL, NULL),
(2, 1, 1, 'smikulic', 'Smikulic1337', 'Siniša', 'Mikulić', 'frut3k@hotmail.com', NULL, 0, NULL, NULL),
(3, 1, 1, 'vmarcetic', 'Vmarcetic1337', 'Vedran', 'Marčetić', 'frut3k@hotmail.com', NULL, NULL, NULL, NULL),
(4, 1, 1, 'nnikolic', 'Nnikolic1337', 'Nenad', 'Nikolić', 'frut3k@hotmail.com', NULL, 0, NULL, NULL),
(5, 1, 1, 'bsipek', 'Bsipek1337', 'Berislav', 'Šipek', 'frut3k@hotmail.com', NULL, 3, NULL, NULL),
(6, 1, 1, 'ireic', 'Ireic1337', 'Ivor', 'Reić', 'frut3k@hotmail.com', NULL, 5, NULL, NULL),
(7, 1, 0, 'mzlataric', 'Mzlataric1337', 'Marin', 'Zlatarić', 'frut3k@hotmail.com', NULL, NULL, NULL, NULL),
(8, 1, 0, 'dcavrak', 'Dcavrak1337', 'Domagoj', 'Čavrak', 'frut3k@hotmail.com', NULL, NULL, NULL, NULL),
(9, 1, 0, 'astimac', 'Astimac1337', 'Alen', 'Štimac', 'frut3k@hotmail.com', NULL, NULL, NULL, NULL),
(10, 1, 0, 'vtopolovec', 'Vtopolovec1337', 'Valentin', 'Topolovec', 'frut3k@hotmail.com', NULL, NULL, NULL, NULL),
(11, 1, 1, 'orokov', 'Orokov1337', 'Ozano', 'Rokov', 'frut3k@hotmail.com', NULL, NULL, NULL, NULL),
(12, 1, 1, 'mhornung', 'Mhornung1337', 'Maja', 'Hornung', 'frut3k@hotmail.com', NULL, NULL, NULL, NULL),
(13, 1, 1, 'otepuric', 'Otepuric1337', 'Ozren', 'Tepurić', 'frut3k@hotmail.com', NULL, NULL, NULL, NULL),
(14, 1, 1, 'cnorris', 'Cnorris1337', 'Chuck', 'Norris', 'frut3k@hotmail.com', NULL, NULL, NULL, NULL),
(15, 1, 1, 'pbishop', 'Pbishop1337', 'Peter', 'Bishop', 'frut3k@hotmail.com', NULL, NULL, NULL, NULL),
(16, 1, 0, 'odunham', 'Odunham1337', 'Olivia', 'Dunham', 'frut3k@hotmail.com', NULL, NULL, NULL, NULL),
(17, 1, 1, 'charper', 'Charper1337', 'Charlie', 'Harper', 'frut3k@hotmail.com', NULL, NULL, NULL, NULL),
(18, 1, 1, 'bstinson', 'Bstinson1337', 'Barney', 'Stinson', 'frut3k@hotmail.com', NULL, NULL, NULL, NULL),
(19, 1, 0, 'ltorvalds', 'Ltorvalds1337', 'Linus', 'Torvalds', 'frut3k@hotmail.com', NULL, NULL, NULL, NULL),
(20, 1, 0, 'dgrohl', 'Dgrohl1337', 'Dave', 'Grohl', 'frut3k@hotmail.com', NULL, NULL, NULL, NULL),
(21, 1, 0, 'gthegrey', 'Gthegrey1337', 'Gandalf', 'The Grey', 'frut3k@hotmail.com', NULL, NULL, NULL, NULL),
(22, 1, 1, 'sgriffin', 'Sgriffin1337', 'Stewie', 'Griffin', 'frut3k@hotmail.com', NULL, NULL, NULL, NULL),
(23, 1, 0, 'tlannister', 'Tlannister1337', 'Tyrion', 'Lannister', 'frut3k@hotmail.com', NULL, NULL, NULL, NULL),
(24, 1, 0, 'rstark', 'Rstark1337', 'Robb', 'Stark', 'frut3k@hotmail.com', NULL, NULL, NULL, NULL),
(25, 1, 1, 'zpodbojec', 'Zpodbojec1337', 'Zvonko', 'Podbojec', 'frut3k@hotmail.com', NULL, NULL, NULL, NULL),
(26, 1, 0, 'Test', 'Test1111', 'Test', 'Test', 'frut3k@hotmail.com', NULL, NULL, NULL, NULL),
(27, 1, 0, 'Pero', 'Pero1337', 'Pero', 'Cečtnik', 'a@a.hr', NULL, NULL, NULL, NULL),
(28, 1, 0, 'novikorisnik', 'Novikorisnik1', 'Novikorisnik', 'Novikorisnik', 'frut3k@hotmail.com', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `predlozak`
--

CREATE TABLE IF NOT EXISTS `predlozak` (
  `id_predlozak` int(10) unsigned NOT NULL,
  `id_korisnik` int(10) unsigned NOT NULL,
  `id_troska` int(10) unsigned DEFAULT NULL,
  `naziv` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id_predlozak`),
  KEY `predlozak_FKIndex1` (`id_korisnik`),
  KEY `predlozak_FKIndex2` (`id_troska`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `status_korisnika`
--

CREATE TABLE IF NOT EXISTS `status_korisnika` (
  `id_status` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `opis` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id_status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tip_korisnika`
--

CREATE TABLE IF NOT EXISTS `tip_korisnika` (
  `id_tip` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `naziv` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id_tip`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tip_korisnika`
--

INSERT INTO `tip_korisnika` (`id_tip`, `naziv`) VALUES
(0, 'admin'),
(1, 'korisnik');

-- --------------------------------------------------------

--
-- Table structure for table `tip_troska`
--

CREATE TABLE IF NOT EXISTS `tip_troska` (
  `id_troska` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `naziv` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id_troska`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=29 ;

--
-- Dumping data for table `tip_troska`
--

INSERT INTO `tip_troska` (`id_troska`, `naziv`) VALUES
(15, 'Hrana'),
(16, 'Zabava'),
(17, 'Auto'),
(18, 'Osobno'),
(19, 'Putovanje'),
(20, 'Stanarina'),
(21, 'Režije'),
(22, 'Kućne potrepštine'),
(23, 'Brucošijada'),
(24, 'Being awesome'),
(25, 'Marketing'),
(26, 'Plaža'),
(27, 'Avansni trošovi'),
(28, 'Naknade');

-- --------------------------------------------------------

--
-- Table structure for table `trosak_clanovi`
--

CREATE TABLE IF NOT EXISTS `trosak_clanovi` (
  `id_trosak_clanovi` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_korisnik` int(10) unsigned NOT NULL,
  `id_troska` int(10) unsigned NOT NULL,
  `godina` int(10) unsigned DEFAULT NULL,
  `mjesec` int(10) unsigned DEFAULT NULL,
  `iznos` float DEFAULT NULL,
  `vrijeme_unosa` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ocjena` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_trosak_clanovi`),
  KEY `trosak_clanovi_FKIndex1` (`id_troska`),
  KEY `trosak_clanovi_FKIndex2` (`id_korisnik`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=65 ;

--
-- Dumping data for table `trosak_clanovi`
--

INSERT INTO `trosak_clanovi` (`id_trosak_clanovi`, `id_korisnik`, `id_troska`, `godina`, `mjesec`, `iznos`, `vrijeme_unosa`, `ocjena`) VALUES
(16, 2, 15, 2012, 6, 35, '2012-06-01 18:53:35', 0),
(17, 2, 15, 2012, 5, 1391, '2012-06-01 18:55:33', 0),
(18, 2, 16, 2012, 5, 401, '2012-06-01 18:56:35', 0),
(19, 2, 17, 2012, 5, 305, '2012-06-01 18:57:34', 0),
(20, 2, 18, 2012, 5, 554, '2012-06-01 18:58:28', 0),
(21, 2, 19, 2012, 5, 338, '2012-06-01 18:58:45', 0),
(22, 2, 17, 2012, 4, 17, '2012-06-01 18:59:06', 0),
(23, 2, 15, 2012, 4, 1180, '2012-06-01 19:00:15', 0),
(24, 2, 16, 2012, 4, 578, '2012-06-01 19:00:31', 0),
(25, 2, 19, 2012, 4, 266, '2012-06-01 19:00:53', 0),
(26, 2, 18, 2012, 4, 735, '2012-06-01 19:01:12', 0),
(27, 2, 20, 2011, 6, 700, '2012-06-01 19:08:13', 0),
(28, 2, 21, 2011, 6, 250, '2012-06-01 19:08:40', 0),
(29, 3, 20, 2011, 6, 700, '2012-06-01 19:10:50', 0),
(30, 3, 21, 2011, 6, 250, '2012-06-01 19:11:07', 0),
(31, 3, 22, 2011, 6, 40, '2012-06-01 19:12:59', 0),
(32, 3, 22, 2011, 4, 0, '2012-06-01 19:14:07', 0),
(33, 3, 22, 2010, 12, 120, '2012-06-01 19:15:06', 0),
(34, 11, 23, 2012, 2, 30000, '2012-06-01 19:20:03', 0),
(35, 11, 23, 2011, 2, 40000, '2012-06-01 19:20:38', 0),
(36, 2, 15, 2012, 1, 1287, '2012-06-01 19:23:14', 0),
(37, 2, 16, 2012, 1, 616, '2012-06-01 19:23:32', 0),
(38, 2, 18, 2011, 12, 1350, '2012-06-01 19:24:11', 0),
(39, 2, 15, 2011, 12, 636, '2012-06-01 19:24:38', 0),
(40, 2, 20, 2010, 1, 700, '2012-06-01 19:26:28', 0),
(41, 2, 21, 2010, 1, 300, '2012-06-01 19:26:44', 0),
(42, 2, 20, 2010, 7, 300, '2012-06-01 19:27:01', 0),
(43, 2, 21, 2011, 1, 700, '2012-06-01 19:27:29', 0),
(44, 4, 20, 2010, 1, 700, '2012-06-01 19:28:14', 0),
(45, 4, 21, 2010, 1, 100, '2012-06-01 19:28:30', 0),
(46, 4, 21, 2010, 2, 500, '2012-06-01 19:28:42', 0),
(47, 4, 20, 2010, 2, 700, '2012-06-01 19:28:52', 0),
(48, 4, 22, 2011, 3, 40, '2012-06-01 19:29:33', 0),
(49, 6, 19, 2011, 8, 5000, '2012-06-01 19:55:53', 0),
(50, 6, 15, 2011, 8, 2000, '2012-06-01 19:56:34', 0),
(51, 6, 16, 2011, 8, 5000, '2012-06-01 19:56:48', 0),
(52, 6, 16, 2011, 9, 10000, '2012-06-01 19:57:07', 0),
(53, 6, 15, 2011, 9, 2000, '2012-06-01 19:57:20', 0),
(54, 6, 16, 2011, 10, 10000, '2012-06-01 19:57:42', 0),
(55, 6, 16, 2011, 11, 15000, '2012-06-01 19:58:11', 0),
(56, 6, 16, 2011, 12, 11000, '2012-06-01 19:58:33', 0),
(57, 6, 16, 2012, 1, 6000, '2012-06-01 19:58:48', 0),
(58, 18, 24, 2012, 6, 5000, '2012-06-01 20:04:39', 0),
(59, 2, 25, 2012, 1, 2200, '2012-06-01 20:08:20', 0),
(60, 2, 21, 2011, 12, 300, '2012-06-01 20:08:49', 0),
(61, 2, 26, 2012, 2, 2100, '2012-06-01 20:09:49', 0),
(62, 3, 15, 2010, 12, 1600, '2012-06-01 20:29:12', 0),
(63, 3, 28, 2011, 2, 800, '2012-06-01 20:30:06', 0),
(64, 3, 28, 2012, 5, 1109, '2012-06-01 20:31:24', 0);

-- --------------------------------------------------------

--
-- Table structure for table `trosak_godina`
--

CREATE TABLE IF NOT EXISTS `trosak_godina` (
  `id_trosak_godina` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_zajednica` int(10) unsigned NOT NULL DEFAULT '0',
  `godina` int(10) unsigned NOT NULL DEFAULT '0',
  `trosak` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_trosak_godina`),
  UNIQUE KEY `godina` (`godina`,`id_zajednica`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=88 ;

--
-- Dumping data for table `trosak_godina`
--

INSERT INTO `trosak_godina` (`id_trosak_godina`, `id_zajednica`, `godina`, `trosak`) VALUES
(40, 32, 2012, 7703),
(51, 31, 2011, 2680),
(56, 31, 2010, 3420),
(57, 34, 2012, 30000),
(58, 34, 2011, 40000),
(61, 32, 2011, 1986),
(72, 35, 2011, 60000),
(80, 35, 2012, 6000),
(81, 38, 2012, 5000),
(82, 39, 2012, 4300),
(83, 39, 2011, 300),
(85, 44, 2010, 1600),
(86, 44, 2011, 800),
(87, 44, 2012, 1109);

-- --------------------------------------------------------

--
-- Table structure for table `trosak_mjesec`
--

CREATE TABLE IF NOT EXISTS `trosak_mjesec` (
  `id_trosak_mjesec` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_zajednica` int(10) unsigned DEFAULT NULL,
  `godina` int(10) unsigned DEFAULT NULL,
  `mjesec` int(10) unsigned DEFAULT NULL,
  `trosak` float DEFAULT NULL,
  PRIMARY KEY (`id_trosak_mjesec`),
  UNIQUE KEY `id_zajednica` (`id_zajednica`,`godina`,`mjesec`),
  KEY `trosak_mjesec_FKIndex1` (`id_zajednica`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=65 ;

--
-- Dumping data for table `trosak_mjesec`
--

INSERT INTO `trosak_mjesec` (`id_trosak_mjesec`, `id_zajednica`, `godina`, `mjesec`, `trosak`) VALUES
(17, 32, 2012, 6, 35),
(18, 32, 2012, 5, 2989),
(23, 32, 2012, 4, 2776),
(28, 31, 2011, 6, 1940),
(33, 31, 2010, 12, 120),
(34, 34, 2012, 2, 30000),
(35, 34, 2011, 2, 40000),
(36, 32, 2012, 1, 1903),
(38, 32, 2011, 12, 1986),
(40, 31, 2010, 1, 1800),
(42, 31, 2010, 7, 300),
(43, 31, 2011, 1, 700),
(46, 31, 2010, 2, 1200),
(48, 31, 2011, 3, 40),
(49, 35, 2011, 8, 12000),
(52, 35, 2011, 9, 12000),
(54, 35, 2011, 10, 10000),
(55, 35, 2011, 11, 15000),
(56, 35, 2011, 12, 11000),
(57, 35, 2012, 1, 6000),
(58, 38, 2012, 6, 5000),
(59, 39, 2012, 1, 2200),
(60, 39, 2011, 12, 300),
(61, 39, 2012, 2, 2100),
(62, 44, 2010, 12, 1600),
(63, 44, 2011, 2, 800),
(64, 44, 2012, 5, 1109);

-- --------------------------------------------------------

--
-- Table structure for table `trosak_ocjene`
--

CREATE TABLE IF NOT EXISTS `trosak_ocjene` (
  `id_trosak_clanovi` int(11) NOT NULL,
  `id_korisnika` int(11) NOT NULL,
  `ocjena` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `trosak_ocjene`
--

INSERT INTO `trosak_ocjene` (`id_trosak_clanovi`, `id_korisnika`, `ocjena`) VALUES
(33, 2, 4);

-- --------------------------------------------------------

--
-- Table structure for table `zajednica`
--

CREATE TABLE IF NOT EXISTS `zajednica` (
  `id_zajednica` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_moderator` int(10) unsigned DEFAULT NULL,
  `naziv` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ukupni_trosak` float DEFAULT '0',
  PRIMARY KEY (`id_zajednica`),
  KEY `zajednica_FKIndex1` (`id_moderator`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=47 ;

--
-- Dumping data for table `zajednica`
--

INSERT INTO `zajednica` (`id_zajednica`, `id_moderator`, `naziv`, `ukupni_trosak`) VALUES
(31, 2, 'Mikulić - Cimeri (VŽ)', 6100),
(32, 2, 'Mikulić - Doma', 9689),
(33, 3, 'Obitelj Marčetić', 0),
(34, 11, 'FOI - Studentski Zbor', 70000),
(35, 6, 'Reić - Bangkok', 66000),
(36, 15, 'Fringe division', 0),
(37, 17, 'Harper - Malibu beach house', 0),
(38, 18, 'Barney - Wait for it - Stinson', 5000),
(39, 2, 'Mikulić- Ured', 4600),
(40, 3, 'Projektni odjel', 0),
(41, 5, 'Obitelj Šipek', 0),
(42, 11, 'Studentski dom', 0),
(43, 11, 'Obitelj Rokov', 0),
(44, 3, 'FOI - Razmjena vještina', 3509),
(45, 1, 'gg2', 0),
(46, 0, 'Isus ', 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
