<?php
	include 'header.php';
	include 'controller/zajednica.php';
	$zajednica = new zajednica();

	if(strstr($_SERVER['REQUEST_URI'], 'action'))
		$zajednica -> $_GET['action']($_GET['id_zajednice']);

	// Print out the value of some variables
	$log->LogDebug("Pristup stranici: _zajednica_c_admin.php");
?>

	<script type="text/javascript">
	$().ready(function() {

		$("input[name='naziv']").keyup(function(){

			var naziv = $(this).val();

			console.log(naziv);

	        var form_data = {
	            'naziv': naziv,
	            'ajax': '1'     
	        };
	        
	        $.post("controller/ajax.php", form_data, function(data){

	        	if(data)
	        	{
	        		$('.error').text(data);
	        		$("input[type='submit']").disabled = true;
	        	} else {
	        		$('.error').text('');
	        		$("input[type='submit']").disabled = false;
	        	}
	        		

	        });
	    });
    });
	</script>


	<div id="wrap">

		<div id="container">
			<div id="content_main">
				<h1>Konfiguracija - Zajednice</h1>
			</div>
			<div id="content_left">
				<h1>Unos nove zajednice</h1>
				<? if(isset($_SESSION['username'])){ ?>
				<form action="_zajednica_c.php?action=upis" method="post" name="forma">
					<table>
						<tr>
							<td class="form_left"><label>Naziv zajednice:</label></td>
						</tr>
						<tr>
							<td><input class="form_right" type="text" name="naziv" id="naziv" value="Naziv..." onfocus="setValue(this)" onblur="setValue(this)"></td>
						</tr>
						<span class="error"></span>
						<tr>
							<td class="form_left"><input class="submit_button" type="submit" value="Unesi"></td>
						</tr>
					</table>
				</form>
				<?} else {?>
				<div>
					<p>Morate biti prijavljeni kako bi unjeli novu zajednicu</p>
					<form action="_zajednica_c.php?login=log_in" method="post" name="forma">
						<table>
							<tr>
								<td class="form_left"><label>Korisničko ime:</label></td>
							</tr>
							<tr>
								<td><input class="form_right" type="text" name="username" id="username" value="Korisničko ime..." onfocus="setValue(this)" onblur="setValue(this)"></td>
							</tr>
							<tr>
								<td class="form_left"><label>Lozinka:</label></td>
							</tr>
							<tr>
								<td><input class="form_right" type="password" name="password" id="password" value="Lozinka..." onfocus="setValue(this)" onblur="setValue(this)"></td>
							</tr>
							<tr>
								<td>
									<input class="submit_button" type="submit" value="Prijava">
								</td>
							</tr>
							<?// echo $_GET['']; ?>
						</table>
					</form>
				</div>
				<?}?>
				<div id="content_left_bottom">

				</div>
			</div>
			<div id="content_right">
					<div id="content_right_bottom">
						
					</div>
			</div>
		</div>
		<div class="clearfooter"></div>
	</div>

	<?php
		$smarty->display('footer.tpl');
		//include 'footer.php';
	?>
