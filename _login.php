<?php ob_start();
	include 'header.php';

	if(strstr($_SERVER['REQUEST_URI'], 'action'))
		$login -> $_GET['action']();
	// Print out the value of some variables
	$log->LogDebug("Pristup stranici: _login.php");
?>

<div id="wrap">

		<div id="container">
			<div id="content_main">
				<h1>Prijava</h1>
				<p>Prijavite se na stranici kako biste imali pristup evidenciji troškova!</p>
			</div>
			<div id="content_left">
				<form action="_login.php?action=log_in" method="post" name="forma">
					<table>
						<tr>
							<td class="form_left"><label>Korisničko ime:</label></td>
						</tr>
						<tr>
							<td><input class="form_right" type="text" name="username" id="username" value="Korisničko ime..." onfocus="setValue(this)" onblur="setValue(this)"></td>
						</tr>
						<tr>
							<td class="form_left"><label>Lozinka:</label></td>
						</tr>
						<tr>
							<td><input class="form_right" type="password" name="password" id="password" value="Lozinka..." onfocus="setValue(this)" onblur="setValue(this)"></td>
						</tr>
						<tr>
							<td>
								<input class="submit_button" type="submit" value="Prijava">
							</td>
						</tr>
					</table>
					<!--<span class="error"></span>-->	
				</form>
				<div id="content_left_bottom">
					<a href="_registracija.php" class="button">Registracija</a>
				</div>
			</div>
			<div id="content_right">
				<h1>Unesite novu zajednicu</h1>
				<a class="button" href="_zajednica_c.php">Kreiraj novu zajednicu</a>
				<div id="content_right_bottom">
					
				</div>
			</div>
		</div>
		<div class="clearfooter"></div>
	</div>

	<?php
		$smarty->display('footer.tpl');
		//include 'footer.php';
	?>
	