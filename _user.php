<?php
	include 'header.php';
	// Print out the value of some variables
	$log->LogDebug("Pristup stranici: _user.php");
?>

	<div id="wrap">
		<div id="container">
			<div id="content_main">
				<h1>Personalizacija sustava</h1>
			</div>
			<div id="content_left">
				<h1>Teme boja</h1>
				<ul id="colorchanger">
					<a class="orange colorbox colororange"></a>
					<a class="darkred colorbox colordarkred"></a>
					<a class="green colorbox colorgreen"></a>
					<a class="blue colorbox colorblue"></a>
				</ul>

				<div id="content_left_bottom">
					
				</div>
			</div>
			<div id="content_right">
				<div id="content_right_bottom">
					
				</div>
			</div>
		</div>
		<div class="clearfooter"></div>
	</div>



	<?php
		$smarty->display('footer.tpl');
		//include 'footer.php';
	?>