<?php
	include 'header.php';	
	include 'controller/troskovi.php';
	include 'controller/zajednica.php';
	$zajednica = new zajednica();
	$troskovi = new troskovi();
	// Print out the value of some variables
	$log->LogDebug("Pristup stranici: _postavke.php");
?>

<script>
  $(function() {
    $( "#accordion" ).accordion();
  });
</script>

	<div id="wrap">
		<div id="container">
			<div id="content_main">
				<h1>Konfiguracija sustava</h1>
			</div>
			<div id="content_left">

				<div id="accordion">
					<h3>Korisnici</h3>
				  	<div>
				  		<ul>
							<a class="button btn-small" href="privatno/korisnici.php">Uređivanje korisnika</a>
							<a class="button btn-small" href="_korisnici_c.php">Dodavanje korisnika</a>
						</ul>
				  	</div>
				  	<h3>Zajednice</h3>
				  	<div>
				  		<ul>
							<a class="button btn-small" href="_zajednica_rud_admin.php">Uređivanje zajednica</a>
							<a class="button btn-small" href="_zajednica_c_admin.php">Dodaj zajednicu</a>
						</ul>
				  	</div>
				  	<h3>Tipovi troška</h3>
				  	<div>
				  		<ul>
							<a class="button btn-small" href="_troskovi_rud_admin.php">Uređivanje kategorije</a>
							<a class="button btn-small" href="_troskovi_c_admin.php">Dodaj kategoriju</a>
						</ul>
				  	</div>
				  	<h3>Teme boja</h3>
				  	<div>
				  		<div id="colorchanger">
							<a class="orange colorbox colororange"></a>
							<a class="darkred colorbox colordarkred"></a>
							<a class="green colorbox colorgreen"></a>
							<a class="blue colorbox colorblue"></a>
						</div>
				  	</div>
				  	<h3>Dnevnik</h3>
				  	<div>
				  		<ul>
							<a class="button btn-small" href="_dnevnik.php">Pregled dnevnika</a>
						</ul>
				  	</div>
				</div>


				<div id="content_left_bottom">
					
					
					
				</div>
			</div>
			<div id="content_right">
				

				<div id="content_right_bottom">
					
				</div>
			</div>
		</div>
		<div class="clearfooter"></div>
	</div>

	<?php
		$smarty->display('footer.tpl');
		//include 'footer.php';
	?>