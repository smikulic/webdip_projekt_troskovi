<?php
	include 'header.php';
	include 'controller/troskovi.php';
	include 'controller/komentari.php';
	$troskovi = new troskovi();
	$komentari = new komentari();

	if(strstr($_SERVER['REQUEST_URI'], 'komentar'))
		$komentari -> $_GET['komentar']();/*
	if(strstr($_SERVER['REQUEST_URI'], 'join'))
		$clanovi-> $_GET['join']();*/

	if(!isset($_SESSION['id']))
	{
		$_SESSION['id'] = '-1';
	}

	// Print out the value of some variables
	$log->LogDebug("Pristup stranici: _trosak_r.php");
?>
<script type="text/javascript">

$().ready(function(){

	$('#star').raty();

	$('.greska').hide();
	var session_provjera = <?= $_SESSION['id'] ?>;
	if (session_provjera == '-1')
	{	
		$('#star').hide();
		$('.greska').show();

	}


	$('#star').click(function(){

	    var form_data = {
	        'ocjena': $('input[name="score"]').val(),
	        'id_trosak_clanovi': '<?= $_GET['id_trosak_clanovi'] ?>',
	        'id_korisnika': <?= $_SESSION['id'] ?>,
	        'ajax': '1'     
	    };
	        
	    $.post("controller/ajax_rate.php", form_data);

	});



	var mjesec = [];

	mjesec.push({ br: '1', naziv: 'Siječanj' });
	mjesec.push({ br: '2', naziv: 'Veljača' });
	mjesec.push({ br: '3', naziv: 'Ožujak' });
	mjesec.push({ br: '4', naziv: 'Travanj' });
	mjesec.push({ br: '5', naziv: 'Svibanj' });
	mjesec.push({ br: '6', naziv: 'Lipanj' });
	mjesec.push({ br: '7', naziv: 'Srpanj' });
	mjesec.push({ br: '8', naziv: 'Kolovoz' });
	mjesec.push({ br: '9', naziv: 'Rujan' });
	mjesec.push({ br: '10', naziv: 'Listopad' });
	mjesec.push({ br: '11', naziv: 'Studeni' });
	mjesec.push({ br: '12', naziv: 'Prosinac' });

	$('.mjesec').each(function(){

		var rb_mjeseca = parseInt($(this).text());

		for(var i=0; i<12; i++){
			
			if(mjesec[i].br == rb_mjeseca){

				$(this).text(mjesec[i].naziv);
			}
		}			
	});
});
<?php echo $id_korisnik; ?>

</script>
<div id="wrap">
		<div id="container">
			<div id="content_main">
				<h1>Troškovi korisnika - <? $troskovi -> ispis_trosak_one($_GET['id_trosak_clanovi']); ?></h1>			
			</div>
			<br />
			<div id="star"></div>
			<div class="greska">Morate biti prijavljeni kako biste ocijenili trošak!</div>
			<div>
				<p>Prosječna ocjena: <? $troskovi -> ispis_ocjena($_GET['id_trosak_clanovi']); ?> </p>
			</div>
			
			<div id="content_left">
				<? $komentari -> upis_prikaz(); ?>

				<div id="content_left_bottom">
					<tr>
			                        <td class="form_left"><label>Ostali komentari:</label></td>
			                    </tr>
					<div class="search_result" style=" background-color: #ffffff; border-radius: 15px; box-shadow: 3px 3px 3px #ccc inset; overflow-y: auto; height: 420px; width: 80%; ">
						<div id="comments">
							<table>
			                    <tr>
			                        <td><? $komentari -> ispis(); ?></td>
			                    </tr>      
		                	</table>
						</div>
					</div>
				</div>
			</div>
			
			<div id="content_right">
				
				<div id="content_right_bottom">
					
				</div>
			</div>
		</div>
		<div class="clearfooter"></div>
	</div>

	<?php
		$smarty->display('footer.tpl');
		//include 'footer.php';
	?>
	