<?php
	include 'header.php';
	include 'controller/troskovi.php';
	$troskovi = new troskovi();

	/*if(strstr($_SERVER['REQUEST_URI'], 'action'))
		$troskovi -> $_GET['action']();
	if(strstr($_SERVER['REQUEST_URI'], 'join'))
		$clanovi-> $_GET['join']();*/

	// Print out the value of some variables
	$log->LogDebug("Pristup stranici: _trosak_clan_r.php");
?>
<script type="text/javascript">

$().ready(function(){

	var mjesec = [];

	mjesec.push({ br: '1', naziv: 'Siječanj' });
	mjesec.push({ br: '2', naziv: 'Veljača' });
	mjesec.push({ br: '3', naziv: 'Ožujak' });
	mjesec.push({ br: '4', naziv: 'Travanj' });
	mjesec.push({ br: '5', naziv: 'Svibanj' });
	mjesec.push({ br: '6', naziv: 'Lipanj' });
	mjesec.push({ br: '7', naziv: 'Srpanj' });
	mjesec.push({ br: '8', naziv: 'Kolovoz' });
	mjesec.push({ br: '9', naziv: 'Rujan' });
	mjesec.push({ br: '10', naziv: 'Listopad' });
	mjesec.push({ br: '11', naziv: 'Studeni' });
	mjesec.push({ br: '12', naziv: 'Prosinac' });

	$('.mjesec').each(function(){

		var rb_mjeseca = parseInt($(this).text());

		for(var i=0; i<12; i++){
			
			if(mjesec[i].br == rb_mjeseca){

				$(this).text(mjesec[i].naziv);
			}
		}			
	});
});

</script>
<div id="wrap">
		<div id="container">
			<div id="content_main">
				<h1>Troškovi</h1>
				
			</div>
			<div id="content_left">
				<h1>Pregled troškova</h1>
				<ul>
					<? $troskovi -> ispis_clan($_GET['id_korisnika']); ?>
				</ul>
				<div id="content_left_bottom">
					
				</div>
			</div>
			<div id="content_right">
				
				<div id="content_right_bottom">
					
				</div>
			</div>
		</div>
		<div class="clearfooter"></div>
	</div>

	<?php
		$smarty->display('footer.tpl');
		//include 'footer.php';
	?>
	