<?php
	include 'header.php';
	// Print out the value of some variables
	$log->LogDebug("Pristup stranici: _zajednica_rud_admin.php");
?>

	<script type="text/javascript">
	    $(document).ready(function () {
	        $('#PersonTableContainer').jtable({
	            title: 'Zajednice',
	            actions: {
	                listAction: 'controller/jtable/list.php',
	                createAction: 'controller/jtable/create.php',
	                updateAction: 'controller/jtable/update.php',
	                deleteAction: 'controller/jtable/delete.php'
	            },
	            fields: {
	                id_zajednica: {
	                    key: true,
	                    list: false
	                },
	                naziv: {
	                    title: 'Naziv',
	                    width: '65%'
	                },
	                ukupni_trosak: {
	                    title: 'Trošak',
	                    width: '35%'
	                }
	            }
	        });
	        $('#PersonTableContainer').jtable('load');
	    });
	</script>

	<div id="wrap">

		<div id="container">
			<div id="content_main">
				<h1>Konfiguracija - Zajednice</h1>
			</div>
			<div id="content_left">
				
				<div id="content_left_bottom">
					
					<div id="PersonTableContainer"></div>
					
				</div>
			</div>
			<div id="content_right" class="edit-naziv">

					<div id="content_right_bottom">
						
					</div>
			</div>
		</div>
		<div class="clearfooter"></div>
	</div>

	<?php
		$smarty->display('footer.tpl');
		//include 'footer.php';
	?>
