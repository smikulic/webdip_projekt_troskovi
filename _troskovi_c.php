<?php
	include 'header.php';
	include 'controller/troskovi.php';
	include 'controller/tiptroska.php';
	include 'controller/clanovi.php';
	$clanovi = new clanovi();
	$troskovi = new troskovi();
	$tiptroska = new tiptroska();

	if(strstr($_SERVER['REQUEST_URI'], 'action'))
		$troskovi -> $_GET['action']();
	if(strstr($_SERVER['REQUEST_URI'], 'tip'))
		$tiptroska -> $_GET['tip']();
	if(strstr($_SERVER['REQUEST_URI'], 'join'))
		$clanovi-> $_GET['join']();

	// Print out the value of some variables
	$log->LogDebug("Pristup stranici: _troskovi_c.php");
?>
	<script src="js/img_upload.js"></script>
<div id="wrap">
<script type="text/javascript">
$().ready(function(){

	$('.search_div').hide();
	$('#id_zajednice').hide();

	$('.anchor_div').click(function(){

		
		$('.search_div').slideToggle("fast");
		$('.search_div').css("height", "200px");

	});

	$('.input_search').keyup(function(){

			var naziv = $(this).val();

			console.log(naziv);

	        var form_data = {
	            'naziv': naziv,
	            'ajax': '1'     
	        };
	        
	        $.post("controller/ajax_search_z.php", form_data, function(data){

	        	if(data)
	        	{
	        		$('.search_result').html(data);
	        		$('.search_result').css("overflow-y", "scroll");
	        		$('.search_result').css("height", "200px");
	        		$('.search_result').css("width", "100%");
	        	} else {
	        		$('.search_result').html('');
	        	}
	        });
	});

});

</script>
		<div id="container">
			<div id="content_main">
				<div class="search_div">
					<input type="input" class="input_search">
					<div class="search_result"></div>
				</div>
				<a class="anchor_div submit_button" href="#">Pretraga zajednica</a>
			</div>


			
			<div id="content_left">
				<h1>Unos novog troška</h1>
				<form action="_troskovi_c.php?action=upis" method="post" name="forma" enctype="multipart/form-data">
					    <div style="float:left; margin-left: 20px;">

					        <label for="img_1">Priložite sliku</label>
					        <input type="file" name="img_1" size="20" />
					        <div id="myDiv" style="width: 240px;"> </div>
					        <p><a href="javascript:;" class="submit_button" onclick="addElement();">Dodajte sliku</a></p>

					    </div>
    <input type="hidden" value="1" id="theValue" name="theValue"/>
					<table>
						<tr>
							<td>
								<select name="godina" id="godina">
									<option value="2010">2010</option>
									<option value="2011">2011</option>
									<option value="2012">2012</option>
									<option value="2013">2013</option>
									<option value="2014">2014</option>
									<option value="2015">2015</option>
									<option value="2016">2016</option>
								</select>
							</td>
							<td>
								<select name="mjesec" id="mjesec">
									<option value="1">Sijećanj</option>
									<option value="2">Veljača</option>
									<option value="3">Ožujak</option>
									<option value="4">Travanj</option>
									<option value="5">Svibanj</option>
									<option value="6">Lipanj</option>
									<option value="7">Srpanj</option>
									<option value="8">Kolovoz</option>
									<option value="9">Rujan</option>
									<option value="10">Listopad</option>
									<option value="11">Studeni</option>
									<option value="12">Prosinac</option>
								</select>
							</td>
							<td>
								<? $tiptroska -> ispis(); ?>
							</td>
						</tr>
						<tr>
							<td class="form_left"><label>Iznos:</label></td>
						</tr>
						<tr>
							<td><input class="form_right" type="text" name="trosak" id="trosak" value="Iznos troška..." onfocus="setValue(this)" onblur="setValue(this)"></td>
							<input type="text" name="id_zajednice" id="id_zajednice" value="<?= $_GET['id_zajednice'] ?>">
							<td><input class="submit_button" type="submit" value="Unesi"></td>
						</tr>
						<!--<span class="error"></span>-->
					</table>
				</form>
				<h1>Unesite novu kategoriju troška</h1>
				<form action="_troskovi_c.php?tip=upis" method="post" name="forma">
					<table>
						<tr>
							<td class="form_left"><label>Nova kategorija troška:</label></td>
						</tr>
						<tr>
							<td><input type="text" name="tiptrosak" id="tiptrosak" value="Kategorija troška..." onfocus="setValue(this)" onblur="setValue(this)"></td>
							<td><input class="submit_button" type="submit" value="Unesi"></td>
						</tr>
					<!--<span class="error"></span>-->
					</table>
				</form>
				<div id="content_left_bottom">
					
				</div>
			</div>
			<div id="content_right">
				
			</div>
		</div>
		<div class="clearfooter"></div>
	</div>

	<?php
		$smarty->display('footer.tpl');
		//include 'footer.php';
	?>
	