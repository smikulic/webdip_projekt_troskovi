<?php
	include 'header.php';
	include 'controller/troskovi.php';
	include 'controller/tiptroska.php';
	include 'controller/clanovi.php';
	$clanovi = new clanovi();
	$troskovi = new troskovi();
	$tiptroska = new tiptroska();

	if(strstr($_SERVER['REQUEST_URI'], 'action'))
		$troskovi -> $_GET['action']();
	if(strstr($_SERVER['REQUEST_URI'], 'tip'))
		$tiptroska -> $_GET['tip']();
	if(strstr($_SERVER['REQUEST_URI'], 'join'))
		$clanovi-> $_GET['join']();

	// Print out the value of some variables
	$log->LogDebug("Pristup stranici: _troskovi_c_admin.php");
?>

<div id="wrap">

		<div id="container">
			<div id="content_main">

			</div>
			
			<div id="content_left">

				<h1>Konfiguracija - Unesite novu kategoriju troška</h1>
				<form action="_troskovi_c.php?tip=upis" method="post" name="forma">
					<table>
						<tr>
							<td class="form_left"><label>Nova kategorija troška:</label></td>
						</tr>
						<tr>
							<td><input type="text" name="tiptrosak" id="tiptrosak" value="Kategorija troška..." onfocus="setValue(this)" onblur="setValue(this)"></td>
							<td><input class="submit_button" type="submit" value="Unesi"></td>
						</tr>
					<!--<span class="error"></span>-->
					</table>
				</form>
				<div id="content_left_bottom">
					
				</div>
			</div>
			<div id="content_right">
				
			</div>
		</div>
		<div class="clearfooter"></div>
	</div>

	<?php
		$smarty->display('footer.tpl');
		//include 'footer.php';
	?>
	