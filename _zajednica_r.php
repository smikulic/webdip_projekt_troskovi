<?php
	include 'header.php';
	include 'controller/zajednica.php';
	include 'controller/clanovi.php';
	$clanovi = new clanovi();
	$zajednica = new zajednica();


	if(strstr($_SERVER['REQUEST_URI'], 'join'))
		$clanovi-> $_GET['join']();

	// Print out the value of some variables
	$log->LogDebug("Pristup stranici: _zajednica_r.php");
?>
<script>
	$(function(){

		$(".graph").hide();

		$( "#datepicker" ).datepicker();
		$( "#datepicker" ).datepicker( "option", "dateFormat", 'dd/mm/yy' );

		$(".plot_graph").click(function(){

			$(".graph").toggle();

		});

	});
</script>
	<div id="wrap">
		<div id="container">
			<div id="content_main">
				<h1>
					<?	if(strstr($_SERVER['REQUEST_URI'], 'action'))
							$zajednica -> $_GET['action']($_GET['id_zajednice']);
						
					?>
				</h1>
				
				
			</div>
			<div id="content_left">
				<h1>Troškovi zajednice</h1>
				<?	$zajednica -> ispis_godine($_GET['id_zajednice']); ?>
				
				<div id="content_left_bottom">
					<h1>Članovi zajednice</h1>
					<?	$zajednica -> clanovi($_GET['id_zajednice']); ?>
					<br />
					<h1>troškovi po datumu</h1>
					<div class="date_from">

						<p>Date: <input type="text" id="datepicker" size="30"/></p>

						</div>

							
								<a href="#" class="plot_graph button" >Prikaži graf</a>
								<li class="graph"><?	$zajednica -> ispis_godine_graph($_GET['id_zajednice']); ?></li>
							
						
					
					
				</div>
			</div>
			<div id="content_right">
				
				<? if(!isset($_SESSION['id'])) { ?>
					<fieldset style="background-color:white ;color: #EC5F01;">
						<legend>Niste logirani:</legend>
						<h1>Ne možete unositi trošak ili se prijaviti u zajednicu dok se ne logirate</h1>
					</fieldset>
				<? } else { ?>

						<? if(isset($_SESSION['id']))
							$clanovi -> provjera();
						?>
					<? } ?>
				<div id="content_right_botom">
					
					<?	if(strstr($_SERVER['REQUEST_URI'], 'action'))
						$zajednica -> moderator($_GET['id_zajednice'], $_SESSION['id']);
					?>
					

				</div>
			</div>
		</div>
		<div class="clearfooter"></div>
	</div>

	<?php
		$smarty->display('footer.tpl');
		//include 'footer.php';
	?>
