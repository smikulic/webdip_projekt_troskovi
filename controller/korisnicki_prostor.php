<?php

class Korisnicki_prostor extends Database{

	function construct() 
    { 	
        parent::__construct(); 
    }

        function ispis_blokirani() {

        	$result = $this -> sql_query_and_where_blokirani();

        	echo '<table border="0" width="100%" cellspacing="0" cellpadding="0">
                <tr>
                    <th align="left">Korisnik &nbsp;&nbsp;&nbsp;</th>
                    <th align="left">broj neuspješnih prijava</th>
                </tr>';

            $i = 0;
	        while($row = mysql_fetch_array($result)){

	            $i++;
	            echo '<tr class=\'red'.($i & 1).'\'>
	                    <td align="justify" style="color: #EC5F01; font-size: 24px; letter-spacing: 1px;" >
	                    <a class="content" href="#">'. $row[3] .'</a>
	                    </td>
	                    <td align="justify" >
	                        ' . $row[9] . ' pokušaja
	                    </td>';
                        if($row[9] >= 3)
                        echo'<td width="80px"><button class="reset_korisnika" id="'.$row[0].'">Reset</button></td>';
                        echo'<td width="80px"><button class="delete_korisnika" id="'.$row[0].'">Briši</button></td>
                    </tr>';
	        }

            echo'</table>';
        }


    function reset_korisnika(){

        $this->sql_update('korisnik', 'neuspjesne_prijave', '0', 'id_korisnik', $_GET['id_korisnik']);
         echo "<script>
                    window.location = 'korisnici.php';
               </script>";
    }

    function odobri_korisnika(){

        $this->sql_update('korisnik', 'id_status', '1', 'id_korisnik', $_GET['id_korisnik']);
         echo "<script>
                    window.location = 'korisnici.php';
               </script>";
    }

    function ban_korisnika(){

        $this->sql_update('korisnik', 'id_status', '0', 'id_korisnik', $_GET['id_korisnik']);
         echo "<script>
                    window.location = 'korisnici.php';
               </script>";
    }

    function delete_korisnika(){

        $this->sql_delete('korisnik', 'id_korisnik', $_GET['id_korisnik']);
         echo "<script>
                    window.location = 'korisnici.php';
               </script>";
    }


    function ispis_korisnika(){

        echo '<div class="admin_wrap well">
                    <table border="0" width="100%" cellspacing="0" cellpadding="0">
                            <tr>
                                <th>Korisničko ime</th>
                                <th>Ime</th>
                                <th>Prezime</th>
                                <th>E-mail</th>
                            </tr>';

        $result = $this->sql_select("korisnik");

        while(($row = mysql_fetch_array($result)) !== false){
                            
        $i++;
                echo '<tr class=\'red'.($i & 1).'\'>
                        <td align="justify" style="color: #EC5F01; font-size: 24px; letter-spacing: 1px;" >'.$row[3].'</td>
                    <td>'.$row[5].'</td>
                    <td>'.$row[6].'</td>
                    <td>'.$row[7].'</td>';
                    if($row[2] == 0)
                    echo'<td width="80px"><button class="odobri_korisnika" id="'.$row[0].'">Odobri</button></td>';
                    else
                    echo'<td width="80px"><button class="ban_korisnika" id="'.$row[0].'">Zabrani</button></td>';
                    echo'<td width="80px"><button class="delete_korisnika" id="'.$row[0].'">Briši</button></td>
                </tr>';
        }
        echo'
            </table><br />
        </div>';
    }

}

?>