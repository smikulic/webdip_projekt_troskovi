<?php

class Zajednica extends Database{

	function construct() 
    { 	
        parent::__construct(); 
    } 

    function ispis(){

        $korisnik = new korisnik();

        echo '<table border="0" width="100%" cellspacing="0" cellpadding="0">
                <tr>
                    <th align="left">Naziv zajednice</th>
                    <th align="left">Ukupni trošak</th>
                    <th align="left">Voditelj</th>
                </tr>';
        $result = $this -> sql_limit_first_sort("zajednica", "10");

        $i = 0;
		while(($row = mysql_fetch_array($result)) !== false){
            $i++;
            echo '<tr class=\'red'.($i & 1).'\'>
                    <td align="justify" style="color: #EC5F01; font-size: 24px; letter-spacing: 1px;" >
                        <a class="content" href="_zajednica_r.php?action=ispis_one&id_zajednice='.$row[0].'">'.$row[2].'</a>
                    </td>
                    <td align="justify" >
                        ' . $row[3] . ' kn
                    </td>
                    <td>
                        ';  if(is_null($row[1]))
                                echo'Nema voditelja';
                            else 
                                $korisnik -> username($row[1]); 
                    echo'</td>
                 </tr>';
		}
    	echo'</table>';
    }
    
    function upis(){
     	
        $data = array(

            'naziv' => $_POST['naziv'],
            'id_moderator' => $_SESSION['id']

        );

        $this -> sql_insert("zajednica", $data);

        $result = $this -> sql_query_and_where("zajednica", "id_moderator", $_SESSION['id'], "naziv", $_POST['naziv']);

            while($row = mysql_fetch_array($result)){

                $data = array(

                    'id_zajednica' => $row[0],
                    'clan' => $_SESSION['id'],
                    'odobren' => '1'
                );

                $this -> sql_insert("clanovi", $data);
            }

		echo "<script>
				window.location = 'index.php';
			  </script>";

     }

     function ispis_one($id_zajednica){

        $result = $this -> sql_query_where("zajednica", "id_zajednica", $id_zajednica);

            while(($row = mysql_fetch_array($result)) !== false){

                echo $row[2];
                /*if(is_null($row[3]))
                    echo'Nema troška';
                else 
                    echo $row[3];*/
            }
    }

    function edit_zajednice(){

        $this->sql_update('zajednica', 'naziv', $_GET['naziv'], 'id_zajednica', $_GET['id_zajednica']);
         echo "<script>
                    window.location = '_zajednica_rud_admin.php';
               </script>";
    }

    function delete_zajednice(){

        $this->sql_delete('zajednica', 'id_zajednica', $_GET['id_zajednica']);
         echo "<script>
                    window.location = '_zajednica_rud_admin.php';
               </script>";
    }

    function ispis_godine($id_zajednica){

        $result = $this -> sql_query_sort_where("trosak_godina", "id_zajednica", $id_zajednica);

        echo '<table border="0" width="100%" cellspacing="0" cellpadding="0">
                <tr>
                    <th align="left">Godina &nbsp;&nbsp;&nbsp;</th>
                    <th align="left">Godišnji trošak</th>
                    <th align="left">Prosječni mjesečni trošak</th>
                </tr>';

        $i = 0;
        while($row = mysql_fetch_array($result)){

            $i++;
            echo '<tr class=\'red'.($i & 1).'\'>
                    <td align="justify" style="color: #EC5F01; font-size: 24px; letter-spacing: 1px;" >
                    <a class="content" href="_trosak_zajednica_r.php?id_zajednica='. $row[1] .'&godina='. $row[2] .'">'. $row[2] .'</a>
                    </td>
                    <td align="justify" >
                        ' . $row[3] . ' kn
                    </td>';


            //echo "Ukupni godišnji trošak je ". $row[3]." Kn za ". $row[2] ." godinu. <br /> Prosječni mjesečni trošak ";
            /*if(is_null($row[3]))
                echo'Nema troška';
            else 
                echo $row[3];*/

            $result_mjesec = $this -> sql_query_and_where("trosak_mjesec", "id_zajednica", $id_zajednica, "godina", $row[2]);

            $izracun = 0;

            while($row = mysql_fetch_array($result_mjesec)){

                $izracun = $izracun + $row['trosak'];

                

            }

            echo '  <td>' .number_format($izracun/12, 2). ' kn
                    </td>
                 </tr>';
        
        

            
           /* $result_mjesec = $this -> sql_query_and_where("trosak_mjesec", "id_zajednica", $id_zajednica, "godina", $row[2]);

            $array = mysql_fetch_array($result_mjesec);

            echo $array['trosak'] . "</br>";*/

        }
        echo'</table><br />';

    }

    function ispis_mjeseca($id_zajednica, $godina){

        echo '<h2>';
        $result = $this -> sql_query_sort_where("trosak_godina", "id_zajednica", $id_zajednica);
        $row = mysql_fetch_row($result);

        $zajednica = new zajednica();
        $zajednica -> naziv_zajednice($row[1]);
        echo '</h2>';

        /*$result = $this -> sql_query_sort_where("trosak_mjesec", "godina", $godina);*/
        $result_mjesec = $this -> sql_query_and_where("trosak_mjesec", "id_zajednica", $id_zajednica, "godina", $godina);
       /* $result = $this -> sql_query_sort_where("trosak_mjesec", "id_zajednica", $id_zajednica); */
       /* $result = $this ->mysql_query("SELECT * FROM trosak_mjesec WHERE id_zajednica='".  $id_zajednica ."'");*/

        echo '<table border="0" width="100%" cellspacing="0" cellpadding="0">
                <tr>
                    <th align="left">Mjesec &nbsp;&nbsp;&nbsp;</th>
                    <th align="left">Mjesečni trošak</th>
                </tr>';

        $i = 0;
        while($row = mysql_fetch_array($result_mjesec)){

            $i++;
            echo '<tr class=\'red'.($i & 1).'\'>
                    <td align="justify" style="color: #EC5F01; font-size: 24px; letter-spacing: 1px;" >
                    <a class="content" href="_trosak_zajednica_r.php?id_zajednica='. $row[1] .'">'. $row[3] .'</a>
                    </td>
                    <td align="justify" >
                        ' . $row[4] . ' kn
                    </td>';


            //echo "Ukupni godišnji trošak je ". $row[3]." Kn za ". $row[2] ." godinu. <br /> Prosječni mjesečni trošak ";
            /*if(is_null($row[3]))
                echo'Nema troška';
            else 
                echo $row[3];*/

            /*$result_mjesec = $this -> sql_query_and_where("trosak_mjesec", "id_zajednica", $id_zajednica, "godina", $row[2]);

            $izracun = 0;

            while($row = mysql_fetch_array($result_mjesec)){

                $izracun = $izracun + $row['trosak'];

                

            }

            echo '  <td>' .number_format($izracun/12, 2). ' kn
                    </td>
                 </tr>';*/
        
        

            
           /* $result_mjesec = $this -> sql_query_and_where("trosak_mjesec", "id_zajednica", $id_zajednica, "godina", $row[2]);

            $array = mysql_fetch_array($result_mjesec);

            echo $array['trosak'] . "</br>";*/

        }
        echo'</table><br />';

    }

    function random_color(){
        mt_srand((double)microtime()*1000000);
        $c = '';
        while(strlen($c)<6){
            $c .= sprintf("%02X", mt_rand(0, 255));
        }
        return $c;
    }


    function ispis_godine_graph($id_zajednica){


        require 'config/lib/GoogleChart.php';
        require 'config/lib/markers/GoogleChartShapeMarker.php';
        require 'config/lib/markers/GoogleChartTextMarker.php';

        $i = 0;
        $chart = new GoogleChart('lc', 600, 300);
        $chart->setAutoscale(true);
        // /$chart->setGridLines(0,20, 3,2);
        $chart->setMargin(5);
            
        

        $chart->setScale(0,$this -> sql_maximum_trosak_mjesec('trosak_mjesec'));

        $y_axis = new GoogleChartAxis('y');
        $y_axis->setDrawTickMarks(false)->setLabels(array(0,
                                                            $this -> sql_maximum_trosak_mjesec('trosak_mjesec')/8,
                                                            $this -> sql_maximum_trosak_mjesec('trosak_mjesec')/6,
                                                            $this -> sql_maximum_trosak_mjesec('trosak_mjesec')/4,
                                                            $this -> sql_maximum_trosak_mjesec('trosak_mjesec')/3,
                                                            $this -> sql_maximum_trosak_mjesec('trosak_mjesec')/2,
                                                            $this -> sql_maximum_trosak_mjesec('trosak_mjesec')
                                                        ));
        $chart->addAxis($y_axis);

        // customize x axis
        $x_axis = new GoogleChartAxis('x');


        /*$x_axis->setDrawTickMarks(false)->setLabels(array(0,'siječanj',
                                                            'veljača',
                                                            'ožujak',
                                                            'travanj',
                                                            'svibanj',
                                                            'lipanj',
                                                            'srpanj',
                                                            'kolovoz',
                                                            'rujan',
                                                            'listopad',
                                                            'studeni',
                                                            'prosinac' ));*/
        $x_axis->setDrawTickMarks(false)->setLabels(array(0,1,2,3,4,5,6,7,8,9,10,11,12));
        $chart->addAxis($x_axis);


        $result = $this -> sql_query_sort_where("trosak_godina", "id_zajednica", $id_zajednica);

        while($row = mysql_fetch_array($result)){

            $graph_array[$i] = array();

            $result_mjesec = $this -> sql_query_and_where_mjesec("trosak_mjesec", "id_zajednica", $id_zajednica, "godina", $row[2]);

            while($row = mysql_fetch_array($result_mjesec)){

                $i = $row['mjesec'];
                $godina[$i] = $row['godina'];
                array_push($graph_array[$i], $row['trosak']);

            }

        }

        for($j = 1; $j < 13; $j++){
            echo $j;
            print_r($graph_array[$j]);

            $data[$j] = new GoogleChartData($graph_array[$j]);
            $data[$j] -> setColor($this -> random_color());
            $data[$j] -> setLegend($godina[$j]);
            $chart->addData($data[$j]);
        
            $shape_marker = new GoogleChartShapeMarker(GoogleChartShapeMarker::CIRCLE);
            $shape_marker->setSize(6);
            $shape_marker->setBorder(2);
            $shape_marker->setData($data[$j]);
            $chart->addMarker($shape_marker);

            $value_marker = new GoogleChartTextMarker(GoogleChartTextMarker::VALUE);
            $value_marker->setData($data[$j]);
            $chart->addMarker($value_marker);

        }

        echo $chart->toHtml();
    }

    function moderator($id_zajednica, $id_korisnika){

        $result = $this -> sql_query_and_where("zajednica", "id_zajednica", $id_zajednica, "id_moderator", $id_korisnika);

        if(mysql_fetch_row($result))
        {

            echo '<h1>Zahtjevi za pristup</h1>
                <table border="0" width="100%" cellspacing="0" cellpadding="0">
                        <tr>
                            <th align="left">Korisnik &nbsp;&nbsp;&nbsp;</th>
                            <th align="left">Zahtjevi</th>
                        </tr>';


            $result_odobri = $this -> sql_query_where("clanovi", "id_zajednica", $id_zajednica);
            $i = 0;
            while($row = mysql_fetch_array($result_odobri)){

                if($row[3] == 0){

                    $result_korisnik = $this -> sql_query_where("korisnik", "id_korisnik", $row[2]);

                    
                    
                    
                    while($row = mysql_fetch_array($result_korisnik)){

                        $i++;
                        echo '<tr class=\'red'.($i & 1).'\'>
                                <td align="justify" style="color: #EC5F01; font-size: 24px; letter-spacing: 1px;" >
                                    ' .$row[3] .'
                                </td>
                                <td align="justify" >
                                    <a class="content" href="_zajednica_r.php?action=odobri&id_zajednice='. $_GET['id_zajednice'] .'&id_korisnika='. $row[0] .'">Odobri</a>
                                </td>
                                <td>
                                    ';  
                                echo'</td>
                             </tr>';

                        /*echo  $row[3] .'
                                  <a class="content" href="_zajednica_r.php?action=odobri&id_zajednice='. $_GET['id_zajednice'] .'&id_korisnika='. $row[0] .'">Odobri</a>
                                  </br>';*/

                    }
                    

                } else {

                    $result_korisnik = $this -> sql_query_where("korisnik", "id_korisnik", $row[2]);

                    
                    
                    
                    while($row = mysql_fetch_array($result_korisnik)){

                        $i++;
                        echo '<tr class=\'red'.($i & 1).'\'>
                                <td align="justify" style="color: #EC5F01; font-size: 24px; letter-spacing: 1px;" >
                                    ' .$row[3] .'
                                </td>
                                <td align="justify" >
                                    <a class="content" href="_zajednica_r.php?action=odobri&id_zajednice='. $_GET['id_zajednice'] .'&id_korisnika='. $row[0] .'">Briši</a>
                                </td>
                                <td>
                                    ';  
                                echo'</td>
                             </tr>';

                        /*echo $row[3] .'
                                  <a class="button_zajednica" href="brisi">Brisi</a></br>';*/

                    }
                    

                }
                

            }
            echo'</table><br />';

        }

    }

    function clanovi($id_zajednica){


            $result_odobri = $this -> sql_query_where("clanovi", "id_zajednica", $id_zajednica);

            echo '<table border="0" width="100%" cellspacing="0" cellpadding="0">
                <tr>
                    <th align="left">Korisnik &nbsp;&nbsp;&nbsp;</th>
                    <th align="left"> </th>
                    <th align="left"> </th>
                </tr>';

         $i = 0;

            while($row = mysql_fetch_array($result_odobri)){

                    $result_korisnik = $this -> sql_query_where("korisnik", "id_korisnik", $row[2]);
                    
                    while($row = mysql_fetch_array($result_korisnik)){

                        $i++;
                        echo '<tr class=\'red'.($i & 1).'\'>
                            <td align="justify" style="color: #EC5F01; font-size: 24px; letter-spacing: 1px;" >
                                <a class="content" href="_trosak_clan_r.php?id_korisnika='. $row[0] .'">'. $row[3] .'</a>
                            </td>';

                        //echo '<a class="content" href="_trosak_clan_r.php?id_korisnika='. $row[0] .'">'. $row[3] .'</a></br>';
            }
        }
        echo '</table><br />';

    }

    function odobri(){

        $this -> sql_update_zajednica_clan("clanovi", $_GET['id_zajednice'], $_GET['id_korisnika']);
    }

    function naziv_zajednice($id_zajednica){

        $result = $this -> sql_query_where("zajednica", "id_zajednica", $id_zajednica);

        while(($row = mysql_fetch_array($result)) !== false){
            echo $row[2];
            //return $row[3];

        }
           
    }
           

}

?>



























