<?php

class Registracija extends Database{

    function construct() 
    {   
        parent::__construct();
    }

    function upis() {

    	require_once('config/recaptchalib.php');
		$privatekey = "6LfiXsMSAAAAAGLOgmFY5RQxUm1QVByvUTrkXdnX";
	  	$resp = recaptcha_check_answer ($privatekey,
		                                $_SERVER["REMOTE_ADDR"],
		                                $_POST["recaptcha_challenge_field"],
		                                $_POST["recaptcha_response_field"]);
		if (!$resp->is_valid) 
	  	{
	    // What happens when the CAPTCHA was entered incorrectly

		   	die ("Niste ispravno unjeli reCAPTCHA polje." .
		       	"(reCAPTCHA said: " . $resp->error . ")");
		} 
		else 
	  	{ 
	  		$password = trim($_POST['lozinka']);
		    $password2 = trim($_POST['lozinka2']);
			$email = $_POST['email'];
			$korisnicko_ime = trim($_POST['korisnicko_ime']);

			$data = array(

	            'korisnicko_ime' => $korisnicko_ime,
	            'lozinka' => $password,
	            'ime' => $_POST['ime'],
	            'prezime' => $_POST['prezime'],
	            'email' => $_POST['email']
        	);
			
	    	if ($password == $password2 && filter_var($email, FILTER_VALIDATE_EMAIL))
			{
				$this -> sql_insert("korisnik", $data);

				$this -> email($email);
				echo '<script>
						alert("Za uspješnu registraciju provjerite email!");
						window.location = "index.php";
					</script>';	
			}
			else
			{
				echo "Pogrešno ste upisali lozinku ili email!";
			}
		}
  	}

  	function upis_admin() {

  		$password = trim($_POST['lozinka']);
	    $password2 = trim($_POST['lozinka2']);
		$email = $_POST['email'];
		$korisnicko_ime = trim($_POST['korisnicko_ime']);

		$data = array(

            'korisnicko_ime' => $korisnicko_ime,
            'lozinka' => $password,
            'ime' => $_POST['ime'],
            'prezime' => $_POST['prezime'],
            'email' => $_POST['email']
    	);

    	$this -> sql_insert("korisnik", $data);
		
  	}

  	function email($email) {

  			$token = strtotime("+1 day");
			$message = "Pozdrav!\n\nUspješno ste se registrirali na Troškovi.hr\n\nDa bi ste aktivirali račun kliknite na link:\n\nhttp://arka.foi.hr/WebDiP/2011_projekti/WebDiP2011_068/controller/aktivacija.php?username=".trim($_POST['korisnicko_ime'])."&token=".$token."\n\nŽelim Vam uspješno korištenje stranice!";
			$message = wordwrap($message, 70);
			$headers = 'From: smikulic@foi.hr' . "\r\n" .
		    'Reply-To: smikulic@foi.hr' . "\r\n" .
		    'X-Mailer: PHP/' . phpversion();
			
			$header_ = 'MIME-Version: 1.0' . "\r\n" . 'Content-type: text/plain; charset=UTF-8' . "\r\n";
			
			mail($email, 'Dobro došli', $message, $header_ . $headers);
  	}
}

	/*if ((($_FILES["uploaded_file"]["type"] == "image/png") || ($_FILES["uploaded_file"]["type"] == "image/jpeg")) && ($_FILES["uploaded_file"]["size"] < 500000))
	{
		if ($_FILES["uploaded_file"]["error"] > 0)
		{
			echo "Greška: " . $_FILES["uploaded_file"]["error"] . "<br />";
		}
		else
		{
			echo "Upload: " . $_FILES["uploaded_file"]["name"] . "<br />";
			echo "Tip datoteke: " . $_FILES["uploaded_file"]["type"] . "<br />";
			echo "Veličina datoteke: " . ($_FILES["uploaded_file"]["size"] / 1024) . " Kb<br />";
			
			$nameFile = $_FILES["uploaded_file"]["name"];
			$typeFile = $_FILES["uploaded_file"]["type"];
			
			
			
			
			
			}
							
		/*	if (file_exists("avatari/" . $_FILES["uploaded_file"]["name"]))
			{
				echo $_FILES["uploaded_file"]["name"] . " Datoteka s tim imenom već postoji. ";
			}
			else
			{
				move_uploaded_file($_FILES["uploaded_file"]["tmp_name"],
				"avatari/" . $_FILES["uploaded_file"]["name"]);
				echo "Spremljeno u: " . "avatari/" . $_FILES["uploaded_file"]["name"];
				echo "<script>alert(\"Uspješno ste se registrirali\");</script>";
			}
		}
				
		list($width,$height) = getimagesize('avatari/'.$nameFile);
		
		$setWidth=100;
		$setHeight=100;
		$res = $width / $height;
		
		if ($setWidth/$setHeight > $res)
		{
			$newWidth = $setHeight*$res;
		    $newHeight = $setHeight;
		}
		else 
		{
			$newHeight = $setWidth/$res;
		    $newWidth = $setWidth;
		}
		
		if ($typeFile == 'image/jpeg')
		{
			$sourceImg = imagecreatefromjpeg('avatari/'.$nameFile);
		}
		else
		{
			$sourceImg = imagecreatefrompng('avatari/'.$nameFile);
		}	
			
			$dimensions=imagecreatetruecolor($newWidth,$newHeight);
			
			imagecopyresampled($dimensions,$sourceImg,0,0,0,0,$newWidth,$newHeight,$width,$height);
			
			$filename = "avatari/". $nameFile;

		if($typeFile == 'image/png')
			imagepng($dimensions,$filename);
		else
			imagejpeg($dimensions,$filename);
		
		imagedestroy($sourceImg);
		imagedestroy($dimensions);
			
		
	}*/


?>