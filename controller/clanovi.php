<?php

class Clanovi extends Database{

	function construct() 
    { 	
        parent::__construct(); 
    } 

    function upis(){

        $id_zajednice = $_GET['id_zajednice'];

        $data = array(

            'id_zajednica' => $_GET['id_zajednice'],
            'clan' => $_SESSION['id']
        );

        $this -> sql_insert("clanovi", $data);
		
        $result = $this -> sql_query_where("zajednica", "id_zajednica", $id_zajednice);

        while(($row = mysql_fetch_array($result)) !== false){
            echo "<script>
                    alert('Prijavili ste se u ". $row[2] ."');
                  </script>";
        }

		echo "<script>
                window.location = '". $_SERVER['HTTP_REFERER'] ."';
              </script>";

    }

    function provjera(){

        $result = $this -> sql_query_and_where("clanovi", "id_zajednica", $_GET['id_zajednice'], "clan", $_SESSION['id']);

        $row = mysql_fetch_array($result);
        if (!mysql_num_rows($result) || $row[3]==0)
        {

            echo '<div id="content_right">
                    <h1>Prijavite se u zajednicu</h1>
                    <a class="button" href="_zajednica_r.php?join=upis&id_zajednice='. $_GET['id_zajednice'] .'">Prijavi se</a>
                  </div>
                  <div id="content_right_bottom">
                      <fieldset style="background-color:white ;color: #EC5F01;">
                        <legend>Niste u zajednici:</legend>
                        <h1>Ne možete unositi trošak dok se ne prijavite u zajednicu</h1>
                      </fieldset>
                  </div>';

        }
        else 
        {
            echo '<h1>Unesite novi trošak</h1><a class="button" href="_troskovi_c.php?id_zajednice='. $_GET['id_zajednice'] .'">&nbsp;Kreiraj novi trošak &nbsp;&nbsp;</a>';

        }


    }
}

?>