<?php

class Komentari extends Database{

	function construct() 
    { 	
        parent::__construct(); 
    }

    function ispis(){

    	 //###############################Ispisuje

        $korisnik = new korisnik();
        ;

        //###############################Korisnika

    	$result = $this -> sql_query_sort_komentari_binary("komentar", "id_trosak_clanovi", $_GET['id_trosak_clanovi']);

        while(($row = mysql_fetch_array($result)) !== false){

        	echo '<h4 style="color: #EC5F01;">' .$korisnik -> username($row[2]). '</h4><div style="border-bottom: groove 1px #EC5F01;">'. $row[4]. '<div style="margin: -20px 0 0 350px">' .$this -> _timeDiff($row[3]).'</div></div>';


        }
    }

    function upis_prikaz(){

    	echo '<form id="registracija" action="_trosak_r.php?komentar=upis&id_trosak_clanovi='. $_GET['id_trosak_clanovi'] .'" method="post" name="forma">
                <table>
                     <tr>
                        <td class="form_left"><label>Komentar:</label></td>
                    </tr>
                    <tr>
                        <td><textarea class="form_right" id="comment" name="comment" value="Komentar..."></textarea></td>
                    </tr>      
                    <tr>
                        <td>
                            <input class="submit_button" type="submit" value="Objava">
                        </td>
                    </tr>
                </table>
    		</form>';

    }

    function upis(){

    	$data = array(

            'id_trosak_clanovi' => $_GET['id_trosak_clanovi'],
            'id_korisnik' => $_SESSION['id'],
            'tekst' => $_POST['comment']
        );

        $this -> sql_insert("komentar", $data);

        echo "<script>
        		window.location = '". $_SERVER['HTTP_REFERER'] ."';
        </script>";

    }

    function _timeDiff($kreirano)
	{
		$dateNow = strtotime(date("Y-m-j G:i:s")); 
		$tempKreirano = strtotime($kreirano);
		
		$kreiranoPrije = $dateNow - $tempKreirano;
		
		$remainingDay     = floor($kreiranoPrije/60/60/24);
		$remainingHour    = floor(($kreiranoPrije-($remainingDay*60*60*24))/60/60);
		$remainingMinutes = floor(($kreiranoPrije-($remainingDay*60*60*24)-($remainingHour*60*60))/60);
				
		if($remainingHour <= 24 && $remainingDay != '0')
		{
			return "prije ". $remainingDay ." dana";
		}
		
		if($remainingDay == '0' && $remainingMinutes > '0' && $remainingHour > '0')
		{
			return "prije ". $remainingHour ." sati";
		}
		if($remainingHour == '0' && $remainingDay == '0' && $remainingMinutes > '0')
		{
			return "prije ". $remainingMinutes ." minuta";	
		}
		if($remainingMinutes == '0' && $remainingHour == '0')
		{
			return "< minute";
		}
		
		//echo "$remainingDay days, $remainingHour hours, $remainingMinutes minutes, $remainingSeconds seconds";
	}
}