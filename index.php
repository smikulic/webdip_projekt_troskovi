<?php
	include 'header.php';	
	include 'controller/troskovi.php';
	include 'controller/zajednica.php';
	$zajednica = new zajednica();
	$troskovi = new troskovi();
	// Print out the value of some variables
	$log->LogDebug("Pristup stranici: index.php");
?>

	<div id="wrap">
		<div id="container">
			<div id="content_main">
				<h1>Dobrodošli!</h1>
				<p>Na stranici troškovi.hr možete kreirati novu zajednicu, evidentirati primitke, izdatke, kao i pregledavati
					statistike vlastitih troškova, kao i troškova zajednica i drugih korisnika</p>
			</div>
			<div id="content_left">
				<h1>Pregled zajednica</h1>
				<ul>
					<? $zajednica -> ispis(); ?>
				</ul>
				<div id="content_left_bottom">
					<h1>Pregled troškova</h1>
				<ul>
					<? $troskovi -> ispis(); ?>
				</ul>
				</div>
			</div>
			<div id="content_right">
				<h1>Unesite novu zajednicu</h1>
				<a class="button fademe" href="_zajednica_c.php">Kreiraj novu zajednicu</a>
				<div id="content_right_bottom">
					<h1>Unesite novi trošak</h1>
					<a class="button fademe2" href="_zajednica_c.php">&nbsp;Kreiraj novi trošak &nbsp;&nbsp;</a>
				</div>
			</div>
		</div>
		<div class="clearfooter"></div>
	</div>

	<?php
		$smarty->display('footer.tpl');
		//include 'footer.php';
	?>