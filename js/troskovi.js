window.onload = function() 
{	
	document.getElementById('korisnicko_ime').addEventListener('focus', fokusirano, true);
	document.getElementById('korisnicko_ime').addEventListener('blur', kimeprovjera, true);
	
	document.getElementById('ime').addEventListener('focus', fokusirano, true);
	document.getElementById('ime').addEventListener('blur', imeprovjera, true);

	document.getElementById('prezime').addEventListener('focus', fokusirano, true);
	document.getElementById('prezime').addEventListener('blur', prezimeprovjera, true);
	
	document.getElementById('lozinka').addEventListener('focus', fokusirano, true);
	document.getElementById('lozinka').addEventListener('blur', lozinkaprovjera, true);

	document.getElementById('lozinka2').addEventListener('focus', fokusirano, true);
	document.getElementById('lozinka2').addEventListener('blur', lozinka2provjera, true);
	
	document.getElementById('email').addEventListener('focus', fokusirano, true);
	document.getElementById('email').addEventListener('blur', mailprovjera, true);
}

function fokusirano() 
{
	this.style.backgroundColor="#ffffff";
}

function prazno(inputField)
{
	inputField.style.backgroundColor = "";
	name = inputField.id + "java";
	helpText = document.getElementById(name);
	return validateRegEx(/.+/, inputField.value, helpText, "Polje ne smije biti prazno!");
}

function kimeprovjera()
{
	if(!prazno(this))
		return false;		
}

function imeprovjera()
{
	if(!prazno(this))
		return false;

	if(!validateRegEx(/^[A-Z]/, this.value, helpText, "Ime ne pocinje velikim slovom!"))
		return false;
}

function prezimeprovjera()
{
	if(!prazno(this))
		return false;

	if(!validateRegEx(/^[A-Z]/, this.value, helpText, "Prezime ne pocinje velikim slovom!"))
		return false;
}


function mailprovjera()
{
	if(!prazno(this))
		return false;

	if(!validateRegEx(/^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/, this.value, helpText, "Primjer: email@host.com"))
		return false;
}

var lozinka1;
var lozinka2;

function lozinkaprovjera()
{
	lozinka1 = this.value;
	if(!prazno(this))
		return false;
	if(!validateRegEx(/(?=.*\d)(?=.*[A-Z]).{8,}/, this.value, helpText, "Lozinka mora imati 8 znakova (veliko slovo i broj)"))
		return false;
}

function lozinka2provjera()
{
	lozinka2 = this.value;
	if(lozinka1 != lozinka2)
	{
		name = this.id + "java";
		helpText = document.getElementById(name);
		helpText.innerHTML = "Ponovljena lozinka nije ista!";
		return false;
	}
	if(!prazno(this))
		return false;	
	if(!validateRegEx(/(?=.*\d)(?=.*[A-Z]).{8,}/, this.value, helpText, "Lozinka mora imati 8 znakova (veliko slovo i broj)"))
		return false;
}

function validateRegEx(regex, input, helpText, helpMessage) 
{
	if (!regex.test(input)) 
	{
		if (helpText != null)
			helpText.innerHTML = helpMessage;
		return false;
	}
	else 
	{
		if (helpText != null)
			helpText.innerHTML = "";
		return true;
	}
}
