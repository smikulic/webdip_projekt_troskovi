<!DOCTYPE html>
<?	error_reporting(0);if (!isset($_SESSION)) { session_start();
	}

	ob_start();

	include 'config/db.php';
	include 'config/openid.php';
 	include 'controller/login.php';
 	include 'controller/openid.php';
 	include 'controller/korisnik.php';
	$login = new login();
	$openid = new openid();

	if(strstr($_SERVER['REQUEST_URI'], '?login'))
		$login -> $_GET['login']();

	// hack version example that works on both *nix and windows
	// Smarty is assumend to be in 'includes/' dir under current script
	define('SMARTY_DIR',str_replace("\\","/",getcwd()).'/Smarty-3.1.10/libs/');

	require_once(SMARTY_DIR . 'Smarty.class.php');
	$smarty = new Smarty();

	$smarty->setTemplateDir('templates/');
	$smarty->setCompileDir('templates_c/');
	$smarty->setConfigDir('configs/');
	$smarty->setCacheDir('cache/');

	$smarty->assign('name','Sinisa');
	$smarty->assign('title','Troškovi.hr');

	require_once 'KLogger.php';

	$log = new KLogger ( "log/log.txt" , KLogger::DEBUG );

	// Do database work that throws an exception
	//$log->LogError("An exception was thrown in header.php");

	// Print out some information
	$log->LogInfo("Korisnik: " . $_SESSION['username']);

	// Print out the value of some variables
	//$log->LogDebug("Korisnik: " . $_SESSION['username']);
?>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<script src="http://code.jquery.com/jquery-latest.js"></script>
	<!--<script type="text/javascript" src="js/jquery-ui-1.8.20.custom.min.js"></script>-->
	<script type="text/javascript" src="js/jquery.raty.min.js"></script>
	<!--<link rel="stylesheet" type="text/css" href="css/custom-theme/jquery-ui-1.8.20.custom.css" media="all" />--> 
	<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.0/themes/base/jquery-ui.css" />
    <script src="http://code.jquery.com/ui/1.10.0/jquery-ui.js"></script>
    <title>Troškovi.hr</title>
    <script type="text/javascript" src="http://www.google.com/jsapi"></script>
    <script src="js/jquery.session.js" type="text/javascript"></script>
    <script src="js/jquery.cookie.js" type="text/javascript"></script>
    <!-- Include one of jTable styles. -->
	<link href="js/jtable/themes/metro/lightgray/jtable.min.css" rel="stylesheet" type="text/css" /> 
	<!-- Include jTable script file. -->
	<script src="js/jtable/jquery.jtable.min.js" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="shortcut icon" href="img/favicon.png">
  	<link rel="apple-touch-icon" sizes="114x114" href="img/icon.png">
  	<script src="http://yui.yahooapis.com/3.8.1/build/yui/yui-min.js"></script>
	<link rel="stylesheet" type="text/css" href="http://yui.yahooapis.com/combo?3.8.1/build/app-transitions-css/app-transitions-css-min.css">
	<script type="text/javascript" src="http://yui.yahooapis.com/combo?3.8.1/build/yui-base/yui-base-min.js&3.8.1/build/oop/oop-min.js&3.8.1/build/attribute-core/attribute-core-min.js&3.8.1/build/event-custom-base/event-custom-base-min.js&3.8.1/build/event-custom-complex/event-custom-complex-min.js&3.8.1/build/attribute-observable/attribute-observable-min.js&3.8.1/build/attribute-extras/attribute-extras-min.js&3.8.1/build/attribute-base/attribute-base-min.js&3.8.1/build/base-core/base-core-min.js&3.8.1/build/base-observable/base-observable-min.js&3.8.1/build/base-base/base-base-min.js&3.8.1/build/features/features-min.js&3.8.1/build/dom-core/dom-core-min.js&3.8.1/build/dom-base/dom-base-min.js&3.8.1/build/dom-style/dom-style-min.js&3.8.1/build/selector-native/selector-native-min.js&3.8.1/build/selector/selector-min.js&3.8.1/build/node-core/node-core-min.js&3.8.1/build/node-base/node-base-min.js&3.8.1/build/event-base/event-base-min.js&3.8.1/build/node-style/node-style-min.js&3.8.1/build/anim-base/anim-base-min.js"></script>
	<script type="text/javascript" src="http://yui.yahooapis.com/combo?3.8.1/build/anim-scroll/anim-scroll-min.js&3.8.1/build/anim-easing/anim-easing-min.js"></script>
</head>
<script type="text/javascript">
	$(function(){
		$.session.set('user_id', '<?php echo $_SESSION["id"]; ?>');
		$('#header_bottom').css('background-color', $.cookie("color-"+ $.session.get('user_id') +""));
		$('#footer_center').css('color', $.cookie("color-"+ $.session.get('user_id') +""));
		$('#footer').css('border-top-color', $.cookie("color-"+ $.session.get('user_id') +""));
		$('.li-decoration').css('color', $.cookie("color-"+ $.session.get('user_id') +""));

		$('.orange').click(function(){
			$.cookie("color-"+ $.session.get('user_id') +"", "#ec5f01", { expires: 7, path: '/' });
			$('#header_bottom').css('background-color', $.cookie("color-"+ $.session.get('user_id') +""));
			$('#footer_center').css('color', $.cookie("color-"+ $.session.get('user_id') +""));
			$('#footer').css('border-top-color', $.cookie("color-"+ $.session.get('user_id') +""));
			$('.li-decoration').css('color', $.cookie("color-"+ $.session.get('user_id') +""));
		});
		$('.green').click(function(){
			$.cookie("color-"+ $.session.get('user_id') +"", "#78A848", { expires: 7, path: '/' });
			$('#header_bottom').css('background-color', $.cookie("color-"+ $.session.get('user_id') +""));
			$('#footer_center').css('color', $.cookie("color-"+ $.session.get('user_id') +""));
			$('#footer').css('border-top-color', $.cookie("color-"+ $.session.get('user_id') +""));
			$('.li-decoration').css('color', $.cookie("color-"+ $.session.get('user_id') +""));
		});
		$('.darkred').click(function(){
			$.cookie("color-"+ $.session.get('user_id') +"", "#c61703", { expires: 7, path: '/' });
			$('#header_bottom').css('background-color', $.cookie("color-"+ $.session.get('user_id') +""));
			$('#footer_center').css('color', $.cookie("color-"+ $.session.get('user_id') +""));
			$('#footer').css('border-top-color', $.cookie("color-"+ $.session.get('user_id') +""));
			$('.li-decoration').css('color', $.cookie("color-"+ $.session.get('user_id') +""));
		});
		$('.blue').click(function(){
			$.cookie("color-"+ $.session.get('user_id') +"", "#90bcd0", { expires: 7, path: '/' });
			$('#header_bottom').css('background-color', $.cookie("color-"+ $.session.get('user_id') +""));
			$('#footer_center').css('color', $.cookie("color-"+ $.session.get('user_id') +""));
			$('#footer').css('border-top-color', $.cookie("color-"+ $.session.get('user_id') +""));
			$('.li-decoration').css('color', $.cookie("color-"+ $.session.get('user_id') +""));
		});
	});

	YUI().use('transition', function (Y) {
		$('.fademe').mouseover(function() {
	    	Y.one('.fademe').transition({
		        duration: 1, // seconds
		        opacity : 0.5
		    });
	    });
	    $('.fademe2').mouseover(function() {
	    	Y.one('.fademe2').transition({
		        duration: 1, // seconds
		        opacity : 0.5
		    });
	    });
	});
</script>
<body>
	<div id="header">

	  	<div id="header_top">
		    <div id="logo">
		      <a href="index.php"><img src="img/logo2.png" alt="troškovi.hr" /></a>
		      <h1 id="logo_h1">Troškovi.hr</h1>
		      <h4 id="logo_h4">Evidencija troškova na razini kućanstva</h4>
		      <? 
		      	if($_SESSION['id'] == '1' && $_SESSION['id'] != '-1') {

		      		if(isset($_SESSION['id']) && $_SESSION['id'] != '-1'){ 
		      ?>
			<div class="login">Dobro došli 
				<a class="login_link" href="privatno/korisnici.php"><?= $_SESSION['username'] ?></a>!&nbsp;
				<a class="login_link" href="_postavke.php">Postavke</a>&nbsp;
				<a class="login_link" style="float:right;" href="_zajednica_c.php?login=logout"> Logout</a></div>
			<?
				} }
				else {

					if(isset($_SESSION['id']) && $_SESSION['id'] != '-1'){
			?>
			<div class="login">Dobro došli <a class="login_link" href="_user.php"><?= $_SESSION['username'] ?></a>!&nbsp;
				<a class="login_link" style="float:right;" href="_zajednica_c.php?login=logout"> Logout</a></div>
			<? } } ?>
		    </div>
		    <div id="menu">
                <div id="menu_right">
                    <ul>
						<li class="li-decoration shrinkme"><a class="menu" href="index.php">Početna</a></li>
				    	<li class="li-decoration"><a class="menu" href="_zajednica_c.php">Zajednice</a></li>
			    		<li class="li-decoration"><a class="menu" href="_troskovi_r.php">Troškovi</a></li>
			    		<li class="li-decoration"><a class="menu" href="_dokumentacija.php">Dokumentacija</a></li>
		    			<li class="li-decoration"><a class="menu" href="_login.php">Prijava/Registracija</a></li>
				    </ul>
                </div>
            </div>
	   	</div>
	   	
	    <div id="header_bottom">
	    	
	  	</div>

	</div>